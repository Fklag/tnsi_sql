 //// coloration syntaxique

function update(text) {
  let result_element = document.querySelector("#highlighting-content");
  // MAJ du code
  result_element.innerHTML = text.replace(new RegExp("&", "g"), "&amp;").replace(new RegExp("<", "g"), "&lt;"); /* Global RegExp */
  // Utilisation de SyntaxHighlight pour colorer
  hljs.highlightElement(result_element);
}

function sync_scroll(element) {
  /* Scroll result to scroll coords of event - sync with textarea */
  let result_element = document.querySelector("#highlighting-content");
  // Get and set x and y
  result_element.scrollTop = element.scrollTop;
  result_element.scrollLeft = element.scrollLeft;
}

function check_tab(element, event) {
    let tabu = "\t".length;
    let code = element.value;
    if(event.key == "Tab") {
    /* si on appuie sur Tab */
    event.preventDefault(); // on neutralise le passage d'un cadre à l'autre
    let before_tab = code.slice(0, element.selectionStart); // texte avant la  tabulation
    let after_tab = code.slice(element.selectionEnd, element.value.length); // texte après  tab
    let cursor_pos = element.selectionEnd + tabu; // on avance le curseur de tabu (3)
    element.value = before_tab + "\t" + after_tab; // on transforme la chaine en intercalant la tabul
    // déplace curseur
    element.selectionStart = cursor_pos;
    element.selectionEnd = cursor_pos;
  }
}
//// fin coloration syntaxique

var output = document.getElementById("output"); // ce qui est répondu
const code = document.getElementById("editing"); // le code entré
const affiche = document.getElementById('affiche');  // savoir si on affiche les entrées dans la sortie :)
let cpt = 0; // compteur de réponses

function addToOutput(s) {
   cpt += 1;
   output.value += 'Rés [' + cpt+ ']: ' + s + '\n';
}

function clearOutput(s) {
   output.value = '';
}

function afficheCommande(s) {
    cpt += 1;
    output.value += 'Ent ['+ cpt+ ']: ' + code.value + '\n';
    output.value += 'Rés [' + cpt+ ']: ' + s + '\n';
}

var clicked = false;
function clique(event){
    clicked = !clicked;
}

if (affiche) {
    affiche.addEventListener("click", clique)};

if (output){
    output.value = 'Un instant Kermaths, je réveille Python...\n';
}
else {
    output = "";
}
 // appel de  Pyodide
async function main(){
    // on attend la réponse de Pyodide
    await loadPyodide({ indexURL : 'https://cdn.jsdelivr.net/pyodide/v0.18.1/full/pyodide.js' });
    output.value = 'Je vous écoute Kermaths\n';
}

let pyodideReadyPromise = main();

// fonction principale appelée par le bouton Exécuter
async function evaluatePython() {
    await pyodideReadyPromise;
    // on attrappe les éventuelles erreurs
   try { // On affiche ou non les commandes dans la sortie
       let output = await pyodide.runPythonAsync(code.value);
       if (clicked) {
	   afficheCommande(output);
       } else {
	   addToOutput(output);
       }
   } catch(err) {
	 addToOutput(err);
   }
 }
