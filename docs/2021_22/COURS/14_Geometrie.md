{{ chapitre(14, "Géométrie : rappels")}}


Cours au format 

- [PDF](../LATEX/PolyGeo21.pdf) 

- et ses sources [TEX](../LATEX/geometrie21.tex)


et un diaporama sur le cours de 2nde:


- [PDF](../LATEX/VecteursBeamer18.pdf) 

- et ses sources [TEX](../LATEX/VecteursBeamer18.tex)
