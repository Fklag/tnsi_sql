{{ chapitre(0, "(Ré)visions estivales")}}

!!! exo ""
	![Placeholder](./IMG/KermitBeach.jpg){ align=right width=250}
	Que  faire  cet  été  sur  la   plage  sinon  un  peu  de  mathématiques  et
	d'informatique ! Ne  perdez pas de temps  à lire des romans  ennuyeux : Visa
	pour la Prépa 2021-2022  est le seul livre que vous  devez emporter. Mais si
	vous n'avez  que votre  smartphone, voici une  petite série  d'activités qui
	devraient égayer vos journées.




## PYTHON

Les installations du  lycée étant...ce qu'elles sont, il serait  bon d'avoir une
bonne installation sur votre propre ordi portable que vous pourrez utiliser au
lycée. L'idéal  serait d'avoir un ordi  tournant sous GNU-Linux :)  Il faudra au
second semestre découvrir aussi le langage SQL. 

`Python` est un vrai langage de programmation. Il est naturellement
disponible  sur  toute  distribution  GNU-Linux et  vous  pouvez  même
l'utiliser sur les autres systèmes d'exploitation.  C'est un
langage de très haut niveau (i.e. il est *loin* de la machine) dont la syntaxe encourage à écrire du code clair et 
de qualité. Dans le domaine de la gestion de la mémoire, les détails de
bas niveau de langages comme le C disparaissent. De plus l'apprentissage de `Python`
est facilité par l'existence d'une interface interactive.  Ceci dit, son intérêt
ne se réduit pas à l'apprentissage de la programmation ou de l'algorithmique;
en témoigne sa popularité croissante. Il a été choisi par des acteurs majeurs:
Google, YouTube, la NASA, etc.  Il
favorise la programmation impérative structurée et la programmation orientée
objet; dans une moindre mesure, il permet de programmer dans un style
fonctionnel. Il est doté
d'un typage dynamique fort, d'une gestion automatique de la mémoire par
ramasse-miettes et d'un système de gestion d'exceptions.  C'est un langage
multi-plateforme, polyvalent (jusque dans les domaines comme le web, les
graphiques, le réseau), open source et gratuit.

En `Python`, tout est objet mais la programmation orientée objet n'est vue que de
manière optionnelle en prépa.


`Python` est accompagné d'un très grand nombre de bibliothèques. Les plus 
utiles   pour  le   programme  de   prépa  seront  `NumPy`,
`SciPy`,   `Matplotlib`, `Sympy`  qui   permettent  de   faire  du   calcul
scientifique.


### Environnement de travail


`Python` est  un langage  qui dispose  d'un interpréteur  très pratique
pour tester de petits bout de code.

On  distinguera donc  un fichier  texte d'extension `.py` dans
lequel on écrira  nos diverses lignes de code  que nous enregistrerons
et pourrons  utiliser à diverses  occasions et nos   « dialogues »
avec  `Python`. Cette  approche intuitive  suffit pour  l'instant: vous
parlerez plus  tard de  langage compilé,  interprété, semi-interprété,
**bytecode**, **opcode**, etc.


Il  existe  de  très  nombreux environnements  de  travail  adaptés  à
`Python`.
Nous ne parlerons que de `iPython` qui est utilisé dans les distributions `Pyzo`
(`https://www.pyzo.org/`) et
  `Spyder` (`https://pythonhosted.org/spyder/`) le  plus souvent disponibles aux
  concours et dans tous les lycées. 


Pour installer `Spyder` par exemple, il suffit d'aller dans un terminal : « l'écran
noir » :)


```shell
python3.X -m pip install spyder 
```

vous remplacerez le X par la version de `Python` disponible sur votre machine et
eventuellement suivi de  `.exe` si vous êtes  sur Windows. Dans la  suite de cet
ouvrage nous utiliserons `Python3.9`. 

```shell
Python 3.9.2 (default, Mar 22 2021, 14:20:33) 
Type 'copyright', 'credits' or 'license' for more information
IPython 7.21.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: 
```

Et voici l'aspect de `Spyder`:

![spyder](./IMG/spyder.png)



Pour des compléments de cours, rendez-vous sur [cette page](../INFORMATIQUE/1_Premiers_Pas/).

Pour des exercices interactifs, rendez-vous sur [celle-ci](https://diraison.github.io/Pyvert/).




## UNE PREMIÈRE SÉRIE D'EXERCICES DΕ MATHÉMATIQUES

Voici  une   [quarantaine  d'exercices](../../EXERCICES/PDF/TSversBCPST_ENONCE.pdf)
pour un  premier entraînement.  Vous ne  traiterez pas  ceux qui  concernent les
nombres  complexes (24,  25, 26).  Des  propositions de  corrigés suivront  plus
tard. Il est très important de les chercher et de rédiger vos réponses.


## QUELQUES CALCULS

!!! {{ exercice()}}

    === "Énoncé"

	    Calculez les dérivées des fonction définies par:
		
		- $f_1(x)=(x^2+x)^3$ 
		- $f_2(x)=(3x^2+4x-6)^4$
		- $f_3(x)=(3x^2+4x-6)^{-4}$
		- $f_4(x)=\frac{1}{(3x^2-5x+1)}$
		- $f_5(x)=\cos(\sqrt{x})$
		- $f_6(x)=\sqrt{\cos(x)}$
		- $f_7(x)=\bigl( \cos(x^2+5x+1) \bigr)^2$
		- $f_8(x)=\cos \bigl( (x^2+5x+1)^2 \bigr)$
		- $f_9(x)=\frac{1}{\cos \bigl( (x^2+5x+1)^2 \bigr)}$
		- $f_{10}(x)=\tan(x)$
		- $f_{11}(x)=\tan \left(\sqrt{x}\right)$
		- $f_{12}(x)=\sqrt{x+\sqrt{x^2+2x+1}}$
		- $f_{13}(x)=(\sqrt{x}+4)(\sqrt{x}-4)$
		- $f_{14}(x)=\frac{\sin(2x)}{\cos(3x)}$
		- $f_{15}(x)=\sqrt{\sqrt{\sqrt{x}}}$
		- $f_{16}(x)=\ln(\ln(\ln(\ln(x))))$

	=== "Indications"

         Vous  pourrez  vérifier  vos  calculs   à  la  machine.  On  obtient  facilement
         l'expression formelle de la dérivée d'une fonction: 
		 
		 ```python
		 In [1]: import sympy as sy
		 
		 In [2]: x,t = sy.symbols('x t')
		 
     	 In [3]: f = lambda x : (x**2 + 3*x)**2
		 
		 In [4]: sy.diff(f(x), x)
         Out[4]: (4*x + 6)*(x**2 + 3*x)

		 ```
 
         On  a deux  solutions  pour faire  de l'expression  de  la dérivée  une
         fonction:
		 
		 ```python
		 In [5]: fp1 = lambda x : sy.diff(f(t), t).subs(t,x)

	     In [6]: fp1(x)
         Out[6]: (4*x + 6)*(x**2 + 3*x)


         In [7]: fp1(t)
         Out[7]: (4*t + 6)*(t**2 + 3*t)

         In [8]: fp1(2)
         Out[8]: 140  
		
		 ```
		 
	    ou

	    ```python
		In [9]: fp2 = sy.lambdify(x, sy.diff(f(x),x))

        In [10]: fp2(x)
        Out[10]: (4*x + 6)*(x**2 + 3*x)

        In [11]: fp2(2)
        Out[11]: 140  
		```
		
		


!!! {{ exercice()}}

    === "Énoncé"

	    Développez et réduisez au maximum les expressions suivantes :
		
		- $e^{x}e^{-x}$
		- $e^{x}e^{-x+1}$
		- $ee^{-x}$
		- $\left(e^{-x}\right)^2$
		- $\dfrac{e^{2x}}{e^{2-x}}$
		- $\dfrac{\left(e^x\right)^3}{e^{2x}}$
		- $e^x \left(e^x+e^{-x}\right)$
		- $\left(e^x\right)^5 \left(e^{-2x}\right)^2$
		- $e^{-3x+1}\left(e^x\right)^3$
		- $\sqrt{e^{-2x}}$
		- $\dfrac{e^{-4x}e}{\left(e^{-x}\right)^2}$
		- $\left(e^x+e^{-x}\right)^2-\left(e^x-e^{-x}\right)^2$
		- $\left(e^x-e^{-x}\right)^2-e^{-x}\left(e^{3x}-e^{-x}\right)$
		- $\left(e^x-e^{-x}\right)\left(e^{2x}+e^x+1\right)$

	=== "Indications"

        - $e^xe^{-x}=e^{x-x}=1$
		- $e^xe^{-x+1}=e^{x-x+1}=e^1=e$
		- $ee^{-x}=e^{1-x}$
		- $(e^{-x})^2=e^{-2x}$
		- $e^{2x}/e^{2-x}=e^{2x-2+x}=e^{3x-2}$
		- $(e^x)^3/e^{2x}=e^{3x-2x}=e^x$
		- $e^x(e^x+e^{-x})=e^{2x}+1$
		- $\left(e^x\right)^5 \left(e^{-2x}\right)^2=e^{5x}e^{-4x}=e^{5x-4x}=e^x$
		- $e^{-3x+1}\left(e^x\right)^3=e^{-3x+1+3x}=e$
		- $\sqrt{e^{-2x}}=e^{-2x/2}=e^{-x}$
		- $\dfrac{e^{-4x}e}{\left(e^{-x}\right)^2}=e^{-4x+1+2x}=e^{-2x+1}$
		- $\left(e^x+e^{-x}\right)^2-\left(e^x-e^{-x}\right)^2=(e^x+e^{-x}+e^x-e^{-x})(e^x+e^{-x}-e^x+e^{-x})=(2e^x)(2e^{-x})=4e^{x-x}=4$
		- $\left(e^x-e^{-x}\right)^2-e^{-x}\left(e^{3x}-e^{-x}\right)=e^{2x}-2e^{x-x}+e^{-2x}-e^{3x-x}+e^{-x-x}=2e^{-2x}-2$
		- $\left(e^x-e^{-x}\right)\left(e^{2x}+e^x+1\right)=e^{3x}+e^{2x}+e^x-e^{x}-1-e^{-x}=e^{3x}+e^{2x}-1-e^{-x}$


!!! {{ exercice()}}

    === "Énoncé"

        Calculez  et  factorisez  les  dérivées et  les  limites  quand  elles existent aux bornes des
		ensembles de définition des fonctions définies par les expressions suivantes :
		
		- $f_1(x)=e^x+x^2+1$
		- $f_2(x)=5e^x+5xe^x$
		- $f_3(x)=\dfrac{e^x+1}{e^x-1}$
		- $f_4(x)=\dfrac{3x+1-e^x}{e^{x}}$
		- $f_5(x)=x^3e^{-x}$
		- $f_6(x)=\dfrac{x^2e^{x}}{x+1}$
		- $f_7(x)=\dfrac{e^{x}}{x}$
		- $f_8(x)=\dfrac{1}{e^x}$
		- $f_{9}(x)=\left(e^x\right)^2+\dfrac{1}{e^x}$
		- $f_{10}(x)=e^{-x}$
		- $f_{11}(x)=e^{4x+1}$
		- $f_{12}(x)=e^{5x^3+7x+4}$
		- $f_{13}(x)=(x+1)e^{-x+1}$
		- $f_{14}(x)=\dfrac{e^{2x}-1}{x}$
		- $f_{15}(x)=e^{\cos(x)}$


	=== "Indications"

         Vérifiez avec Python. Par exemple :
		 
		 ```python
		 In [169]: f1 = lambda x : sy.exp(x) + x**2 + 1

         In [170]: sy.limit(f1(x), x, sy.oo)
         Out[170]: oo

         In [171]: sy.limit(f1(x), x, -sy.oo)
         Out[171]: oo

         In [172]: sy.diff(f1(x), x)
         Out[172]: 
                x
         2.x + e 
		 ```

