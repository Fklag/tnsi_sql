{{ chapitre(25, "Variables aléatoires sur un univers fini")}}


Cours au format 

- [PDF](../LATEX/PolyVar21.pdf) 

- et ses sources [TEX](../LATEX/VAR21.tex)
