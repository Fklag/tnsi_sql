//const config = {
//  type: 'line',
//  data: data,
//};

var lab = []
var dat= []

//var lab = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
function deff(f, a, b, N) {
    lab = []
    dat= []
    const h = (b - a)/N
    for (var j = 0; j < N; j++) {//j <lab.length; j++) {
	x = a + j*h;
	lab.push(x); //x = lab[j],
	y = f(x);
	dat.push(y);
    }
    //console.log(lab);
}


deff(t => 1/(3.14*(1+t*t)), -5, 5, 100);

var data5 = {
    labels: lab,
    datasets: [
        {
	    borderColor: "rgba(250,150,0,0.5)",
	    radius: 0,
	    fill:false,
	    cubicInterpolationMode: 'monotone',
            data:dat
        }
    ]
};


deff(t => 0.5+Math.atan(t)/3.14 , -5, 5, 100);

var data6 = {
    labels: lab,
    datasets: [
        {
	    borderColor: "rgba(250,150,0,0.5)",
	    radius: 0.5,
	    fill:false,
	    cubicInterpolationMode: 'monotone',
            data:dat
        }
    ]
};


deff(t => Math.exp(-t*t/2)/Math.sqrt(6.28) , -5, 5, 100);

var data7 = {
    labels: lab,
    datasets: [
        {
	    borderColor: "rgba(250,150,0,0.5)",
	    radius: 0.5,
	    fill:false,
	    cubicInterpolationMode: 'monotone',
            data:dat
        }
    ]
};



var ctx5 = document.getElementById("myChart5").getContext("2d");
var ctx6 = document.getElementById("myChart6").getContext("2d");
var ctx7 = document.getElementById("myChart7").getContext("2d");


var stackedLine5 = new Chart(ctx5, {
    type: 'line',
    data: data5,
    options: {
	aspectRatio: 2,
        scales: {
	   
            y: {
                stacked: true,
		display: true
            },
	    x: {
		min:-5,
		stacked: true,
		display: true,
		ticks: {
		    suggestedMax: 5,
		    suggestedMin: -5,
		    callback: function(val) {
		    	let l = Math.floor(this.getLabelForValue(val));
		    	let v = this.getLabelForValue(val);
		    	//console.log(v);
		    	return l === v ? this.getLabelForValue(val) : "";
		    }
		}
	    }
        },
	plugins: {
	     legend: {
		display:false
	     },
	    title: {
		display: true,
		text: 'Densité de probabilité'
	    }
	}
    }
});



///////////////////////////////



var stackedLine6 = new Chart(ctx6, {
    type: 'line',
    data: data6,
    options: {
	aspectRatio: 2,
        scales: {
	   
            y: {
                stacked: true,
		display: true
            },
	    x: {
		min:-5,
		stacked: true,
		display: true,
		ticks: {
		    suggestedMax: 5,
		    suggestedMin: -5,
		    callback: function(val) {
		    	let l = Math.floor(this.getLabelForValue(val));
		    	let v = this.getLabelForValue(val);
		    	//console.log(v);
		    	return l === v ? this.getLabelForValue(val) : "";
		    }
		}
	    }
        },
	plugins: {
	     legend: {
		display:false
	    },
	    title: {
		display: true,
		text: 'Fonction de répartition'
	    }
	}
    }
});




///////////////////////////////



var stackedLine7 = new Chart(ctx7, {
    type: 'line',
    data: data7,
    options: {
	aspectRatio: 2,
        scales: {
	   
            y: {
                stacked: true,
		display: true
            },
	    x: {
		min:-5,
		stacked: true,
		display: true,
		ticks: {
		    suggestedMax: 5,
		    suggestedMin: -5,
		    callback: function(val) {
		    	let l = Math.floor(this.getLabelForValue(val));
		    	let v = this.getLabelForValue(val);
		    	//console.log(v);
		    	return l === v ? this.getLabelForValue(val) : "";
		    }
		}
	    }
        },
	plugins: {
	     legend: {
		display:false
	    },
	    title: {
		display: true,
		text: 'Courbe de Gauss'
	    }
	}
    }
});
