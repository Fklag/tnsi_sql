%% Last modified: <fdxvars21.tex modifié par  Guillaume CONNAN le dimanche 22 mai 2022 à 17h 34min 46s>


\chapterimage{fawcett}
\chaptertext{Philippa \textsc{Fawcett} est  née en 1868 à  Brighton. Fille d'une
  suffragette,  nièce de  la première  femme docteur  en médecine,  elle est  la
  première  femme à  être classée  au-dessus de  tous les  hommes à  l'examen de
  mathématiques  de fin  de licence  à l'Université  de Cambridge.  Malgré cela,
  étant une  femme, elle ne pu  recevoir le titre honorifique  de \textit{senior
    wrangler}  qui revient  au  vainqueur  de ce  concours.  Elle fut  cependant
  admise  comme  professeur  et  poursuivit des  recherches  notamment  dans  le
  domaine de la mécanique des fluides:
%\begin{figure}
\begin{center}
  \includegraphics[width=\linewidth]{fawcett2}
\end{center}
% \end{figure}
Vous  pouvez retrouver  les notations  de  dérivation partielle  que vous  aviez
découvertes avec  M. \textsc{Michalewicz}. Nous  ne ferons que le  rapide survol
des notions que nous laisse étudier le programme de \textsc{Bcpst}.
}

\chapter{Fonctions de 2 variables}


\section{Continuité et représentations graphiques}

\subsection{Représentations graphiques de fonctions à deux variables}
\begin{definition}[Pavé ouvert du plan]

On appelle pavé ouvert du plan, toute partie de $\R^2$ de la forme $I\otimes J$, où $I$ et $J$ sont des intervalles ouverts de $\R$ (de la forme $]a,b[$ avec $a,b\in\overline{\R}$).
\end{definition}

\begin{recherche}[Application]

 Représenter graphiquement les pavés ouverts $]-1,2[ \otimes ]-3,1[$, $\R^{+*}\otimes\R^{+*}$ et $]0,1[^2$.
\end{recherche}

Pour obtenir la  représentation graphique d'une fonction de  deux variables avec
Python, on peut procéder ainsi:

\begin{pythoncode}
import numpy as np
import matplotlib.pyplot as plt

# la fonction
def f(x, y):
   return ... # la définition de la fonction en utilisant les fonctions de numpy
	
# le domaine de définition
x = np.linspace(deb_x, fin_x, 100)
y = np.linspace(deb_y, fin_y, 100)

# on transforme ces deux tableaux en maillage horizontal
X, Y = np.meshgrid(x, y)

# on crée le maillage de la surface à tracer
Z = f(X, Y)

# on trace
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot_wireframe(X, Y, Z, color='blue') # ou plot_surface
ax.set_title('$f(x,y)=\sqrt{2 - x^2 - y^2}$')
plt.show()
\end{pythoncode}

\begin{definition}[Surface représentative d'une fonction de deux variables]

Soient $\mathscr{D}$ un pavé ouvert du plan et $f:\left\lbrace\begin{array}{ccl}\mathscr{D} & \mapsto & \R \\ (x,y) & \mapsto & f(x,y)\end{array}\right.$ 
une fonction de deux variables définie sur $\mathscr{D}$.

La surface représentative $\mathscr{S}_f$ de $f$ dans un repère de l'espace est l'ensemble des points $M(x,y,z)$ vérifiant 
$z=f(x,y)$, avec $(x,y)\in\mathscr{D}$:

\[ \mathscr{S}_f=\Big\{M(x,y,z): (x,y)\in\mathscr{D}, z=f(x,y)\Big\} \]

\end{definition}

\begin{recherche}

On considère la fonction $f(x,y)=\sqrt{2-x^2-y^2}$.

\begin{center}
\includegraphics[scale=0.65]{fx2var1.png}

\end{center}

\begin{enumerate}
	\item Déterminer le domaine de définition de $f$.
	\item Quelle est l'intersection de $\mathscr{S}_f$ avec le plan $(xOz)$?
	\item Quelle est l'intersection de $\mathscr{S}_f$ avec le plan d'équation $z=1$?
\end{enumerate}

\end{recherche}


\begin{remarque}[Coupes par des plans]

L'intersection de la surface $\mathscr{S}_f$ avec le plan d'équation $x=x_0$ (respectivement $y=y_0$) est la courbe représentative de la fonction partielle $f_{x_0}:y\mapsto f(x_0,y)$ (respectivement $f_{y_0}:x\mapsto f(x,y_0)$) dans ce plan.

\end{remarque}

\begin{definition}[Ligne de niveau]

Soit $f:\mathscr{D} \rightarrow \R^2$ une fonction de deux variables et $\mathscr {S}_f$ sa surface représentative.

Si $k\in\R$, on appelle courbe (ou ligne) de niveau $k$ de la surface $\mathscr{S}_f$, l'ensemble des points $N(x,y)$ du plan $(xOy)$ vérifiant:

\medskip

 \centerline{$(x,y)\in\mathscr{D}$ et $f(x,y)=k$.}

\end{definition}

\begin{recherche} Déterminer les lignes de niveau de la surface $\mathscr{S}$ d'équation $z=xy$:


\begin{center}
\includegraphics[scale=0.65]{./IMG/fx2var2.png}
\end{center}

\end{recherche}

\bclampe On a utilisé \mintinline{python}{ax.contour3D(X, Y, Z, 50, cmap='viridis')}



\subsection{Approche intuitive de la continuité}

\begin{remarque}[Rigueur mathématique]

La partie qui suit s'appuie sur la notion de limite dans $\R^2$. Celle-ci n'a pas été définie dans le cours (et pour cause \dots), aussi les définitions qui suivent sont principalement à titre informatif.
\end{remarque}

\begin{definition}[Fonction continue en un point ; sur un pavé]

Soit $f$ une fonction de deux variables définie sur un pavé ouvert $\mathscr{D}$.

On dit que $f$ est continue en $(x_0,y_0)\in\mathscr{D}$ si $\lim_{(x,y)\to(x_0,y_0)}f(x,y)=f(x_0,y_0)$.

On dit que $f$ est continue sur $\mathscr{D}$ si $f$ est continue en tout point $(x_0,y_0)$ de $\mathscr{D}$. On note $\mathscr{C}^0(\mathscr{D})$ l'ensemble des fonctions continues sur $\mathscr{D}$.
\end{definition}

\begin{recherche}[Application]

Dans chaque cas, étudier la continuité de $f$:

\medskip

\centerline{a) $f(x,y)=3x^2-2y^2+xy+1$; \hskip 1cm b) $f(x,y)=x^y$.}

\end{recherche}


\begin{theoreme}[Continuité et opérations]


\begin{enumerate}
	\item Une combinaison linéaire, un produit ou un quotient (dont le dénominateur ne s'annule pas) de fonctions de deux variables continues est une fonction de deux variables continue. 
	\item En particulier, les fonctions polynômes et rationnelles de deux variables sont continues sur leur domaine de définition.
	\item Si $f:I\otimes J\mapsto K\subset\R$ et $u:K\mapsto\R$ sont continues alors $u\circ f$ est continue sur $I\otimes J$.
	
	\item Si $f$ est continue sur $I\otimes J$ alors les fonctions partielles $f_y:x\mapsto f(x,y)$ et $f_x:y\mapsto f(x,y)$ sont continues sur $I$ et $J$ respectivement. La réciproque est fausse.
\end{enumerate}

\end{theoreme}

\section{Dérivées partielles d'ordre 1}

\subsection{Gradient ; fonctions de classe $\mathscr{C}^1$}

\begin{definition}[Définition des dérivées partielles]

Soit $f:\left\lbrace\begin{array}{ccl} I\otimes J & \mapsto & \R \\ (x,y) & \mapsto & f(x,y)\end{array}\right.$ 

\begin{enumerate}
	\item On dit que $f$ admet une dérivée partielle (d'ordre $1$) par rapport à sa première variable sur $I\otimes J$ si pour tout $y\in J$, la fonction partielle $f_y:x\mapsto f(x,y)$ est dérivable sur $I$. Dans ce cas, cette dérivée partielle est définie par:
	
\[ \frac{\partial f}{\partial x}(x,y)=f_y'(x)=\lim_{h\rightarrow 0}\frac{f(x+h,y)-f(x,y)}{h} \]
	
	
	\item On dit que $f$ admet une dérivée partielle (d'ordre $1$) par rapport à sa deuxième variable sur $I\otimes J$ si pour tout $x\in I$, la fonction partielle $f_x:y\mapsto f(x,y)$ est dérivable sur $J$. Dans ce cas, cette dérivée partielle est définie par:

\[ \frac{\partial f}{\partial y}(x,y)=f_x'(y)=\lim_{h\rightarrow 0}\frac{f(x,y+h)-f(x,y)}{h} \]
\end{enumerate}

\end{definition}

\begin{definition}[Gradient]

Soit $f$ définie sur un pavé ouvert $\mathscr{D}$.

Si $f$ admet des dérivées partielles d'ordre $1$ sur $\mathscr{D}$, on appelle gradient de $f$ en $(x_0,y_0)\in\mathscr{D}$ le vecteur de $\R^2$ défini par:
	
\[ \overrightarrow{\text{grad}}f(x_0,y_0)=\left(\frac{\partial f}{\partial x}(x_0,y_0),\frac{\partial f}{\partial y}(x_0,y_0)\right) \]

\end{definition}

\begin{definition}[Fonction de classe $\mathscr{C}^1$]

Soit $f$ définie sur un pavé ouvert $\mathscr{D}$.	
Si $f$ admet des dérivées partielles d'ordre $1$ sur $\mathscr{D}$ qui sont continues, on dit que $f$ est de classe $\mathscr{C}^1$ sur $\mathscr{D}$. On note $\mathscr{C}^1(\mathscr{D})$ l'ensemble des fonctions de classe $\mathscr{C}^1$ sur $\mathscr{D}$.

\end{definition}

\begin{recherche}[Application]

Dans chaque cas, montrer que $f$ est de classe $\mathscr{C}^1$ sur $\R^2$ et déterminer son gradient en tout point de $\R^2$:

\medskip

\centerline{a) $f(x,y)=5x^2-2xy+2y^2$; \hskip 1cm b) $f(x,y)=x^2\sin(y)$.}

\end{recherche}


\subsection{Approximation locale d'une fonction $\mathscr{C}^1$}

\begin{theoreme}{Approximation locale d'une fonction $\mathscr{C}^1$}

Soit $f\in\mathscr{C}^0(\mathscr{D})$ et $(x_0,y_0)\in\mathscr{D}$. Alors, au voisinage de $(x_0,y_0)$,

\medskip

\[ f(x,y)-f(x_0,y_0)\approx \frac{\partial f}{\partial x}(x_0,y_0)(x-x_0)+\frac{\partial f}{\partial y}(x_0,y_0)(y-y_0) \]

\end{theoreme}

\begin{remarque}[Interprétation graphique : plan tangent]

Ce résultat permet d'évaluer une petite variation de la valeur d'une fonction $f$ de classe $\mathscr{C}^0$ découlant de petites variations des variables. 

La fonction $(x,y)\mapsto\displaystyle{f(x_0,y_0)+\frac{\partial f}{\partial x}(x_0,y_0)(x-x_0)+\frac{\partial f}{\partial y}(x_0,y_0)(y-y_0)}$ est une approximation de $f$ au voisinage de $(x_0,y_0)$ et sa surface représentative est le plan tangent à celle de $f$ au point $(x_0,y_0,f(x_0,y_0))$. 


\end{remarque}

\begin{figure}[!bht]
\begin{center}
\includegraphics[scale=0.65]{./IMG/fx2var3.png}
\caption{\label{fig3} Représentation graphique de la surface représentative de $f(x,y)=(x^2+y^2)/2$ et du plan tangent en $(1/2,1/2)$}
\end{center}
\end{figure}
 

\begin{recherche}[Application]

Soit $(x_0,y_0) \in \R^2$. Déterminer le plan tangent à la surface représentative de $f(x,y)=3x^2-2y^2+xy+1$ en $M(x_0,y_0,f(x_0,y_0))$ 
\end{recherche}


\subsection{Extréma d'une fonction de classe $\mathscr{C}^1$}

\begin{theoreme}[Condition nécessaire pour avoir un extrémum d'une fonction $\mathscr{C}^1$]

Soit $f$ définie sur un pavé ouvert $\mathscr{D}$ de $R^2$, admettant des dérivées partielles d'ordre $1$ sur $\mathscr{D}$. Si $f$ admet un extremum en $(x_0,y_0)\in\mathscr{D}$, alors:

\[ \frac{\partial f}{\partial x}(x_0,y_0)=0 \text{ et } \frac{\partial f}{\partial y}(x_0,y_0)=0 \]

\end{theoreme}

\bcdanger La réciproque est fausse!

\begin{figure}[!bht]
\begin{center}
\includegraphics[scale=0.65]{./IMG/fx2var4.png}
\caption{\label{fig3} Représentation graphique de la surface représentative de $f(x,y)=x^2-y^2$}
\end{center}
\end{figure}

Avec $f(x,y)=x^2-y^2$, on a $\dfrac{\partial f}{\partial x}(0,0)=0$ et $\dfrac{\partial f}{\partial y}(0,0)=0$ mais il n'y a pas d'extrémum en $(0,0)$.



\begin{recherche}[Application]

Soit $f$ définie sur $(\R^{+*})^2$ par $\displaystyle{f(x,y)=xy+\frac{1}{x}+\frac{1}{y}}$.

Montrer que $f$ admet un minimum, à déterminer.

\end{recherche}


\begin{recherche}[Retour sur l'ajustement affine par la méthode des moindres carrés]

Soit $(x_i,y_i)_{1\leq i\leq n}$ une série statistique double d'effectif total $n$. On cherche un bon ajustement affine de cette série statistique. Pour cela, on pose:

\[ f(a,b)=\frac{1}{n}\sum_{i=1}^n\left[y_i-(ax_i+b)\right]^2 \]

Déterminer les solutions de l'équation vectorielle $\displaystyle{\overrightarrow{\text{grad}}}f(a,b)=0_{\R^2}$ puis interpréter le résultat.

\end{recherche}

\subsection{Dérivation d'une expression de la forme $f(x(t),y(t))$}

\begin{theoreme}[Dérivation d'une expression de la forme $f(x(t),y(t))$]

Soit $f\in\mathscr{C}^1(\mathscr{D})$ et $x,y:I\mapsto\R$ dérivables sur $I$ telles que $\forall t\in I$, $(x(t),y(t))\in\mathscr{D}$.

Alors la fonction $\varphi:t\mapsto f(x(t),y(t))$ est dérivable sur $I$ et $\forall t\in I$:

\[ \varphi'(t)=\frac{\partial f}{\partial x}(x(t),y(t))x'(t)+\frac{\partial f}{\partial y}(x(t),y(t))y'(t) \]

On peut ainsi écrire $\varphi'(t)=\displaystyle{\overrightarrow{\text{grad}}f(x(t),y(t))\cdot(x'(t),y'(t))}$.

\end{theoreme}

\begin{recherche}[Application]

On pose $f(x,y)=x^2+y^2$. Déterminer la dérivée de $\varphi:t\mapsto f(a\cos(t),b\sin(t))$.

\end{recherche}


\section{Dérivées partielles d'ordre 2}

\begin{definition}[Dérivées partielles secondes]

Soit $f$ définie sur un pavé ouvert $\mathscr{D}$, admettant des dérivées partielles sur $\mathscr{D}$.

\medskip

Si $\displaystyle{\frac{\partial f}{\partial x}}$ et $\displaystyle{\frac{\partial f}{\partial y}}$ admettent des dérivées partielles en $(x_0,y_0)\in\mathscr{D}$, on dit que $f$ admet des dérivées partielles d'ordre $2$ en $(x_0,y_0)$, notées:

\bigskip

\noindent \hskip 1cm $\displaystyle{\frac{\partial^2 f}{\partial x^2}(x_0,y_0)=\frac{\partial f}{\partial x}\left(\frac{\partial f}{\partial x}\right)(x_0,y_0)}$,\hskip 1.2cm $\displaystyle{\frac{\partial^2 f}{\partial y\partial x}(x_0,y_0)=\frac{\partial f}{\partial y}\left(\frac{\partial f}{\partial x}\right)(x_0,y_0)}$,

\bigskip

\noindent \hskip 1cm $\displaystyle{\frac{\partial^2 f}{\partial x\partial y}(x_0,y_0)=\frac{\partial f}{\partial x}\left(\frac{\partial f}{\partial y}\right)(x_0,y_0)}$,\hskip 1cm $\displaystyle{\frac{\partial^2 f}{\partial y^2}(x_0,y_0)=\frac{\partial f}{\partial y}\left(\frac{\partial f}{\partial y}\right)(x_0,y_0)}$.

\end{definition}


\begin{theoreme}[Théorème de Schwarz]

 Soit $f$ définie sur un pavé ouvert $\mathscr{D}$, admettant des dérivées partielles d'ordre $2$ sur $\mathscr{D}$. Si les dérivées partielles d'ordre $2$ de $f$ sont continues en $(x_0,y_0)\in\mathscr{D}$ alors

\[ \frac{\partial^2 f}{\partial y\partial x}(x_0,y_0)=\frac{\partial^2 f}{\partial x\partial y}(x_0,y_0) \]

\end{theoreme}


\begin{recherche}[Application]

Soit $f$ définie sur $\R^2$ par $f(x,y)=\dfrac{xy(x^2-y^2)}{x^2+y^2}$ si $(x,y)\neq(0,0)$ et $f(0,0)=0$. 

Montrer que $f$ admet des dérivées partielles d'ordre $2$ en $(0,0)$, à déterminer. Que peut-on en déduire?

\end{recherche}

\begin{recherche}[GEE 2004]

  On considère le système d'équations différentielles:

  \[
(1)
\begin{cases}
  \dfrac{{\rm d}x}{{\rm d}t}=y\\
  \dfrac{{\rm d}y}{{\rm d}t}=-x+x^3\\

\end{cases}
  \]

  où $x$  et $y$ sont  deux fonctions dérivables de  la variable réelle  $t$, de
  dérivées respectives  $\dfrac{{\rm d}x}{{\rm  d}t}$ et  $\dfrac{{\rm d}y}{{\rm
      d}t}$.

  \begin{enumerate}
  \item Identifier les solutions constantes de (1).

  \item On considère maintenant la fonction $V$ des variables réelles $x$ et $y$
    définie par:
    \[
V(x,y)=x^2+y^2-\frac{x^4}{2}
\]
Calculer les dérivées  partielles de $V$ et déterminer les  points pour lesquels
le gradient de V est nul.
\item Démontrer que si  $x$ et $y$ sont deux solutions de (1)  sur $ℝ$, alors la
  fonction composée $t\mapsto V(x(t), y(t))$ de l avariable $t$ est constante.
\item  Tracer le  graphe de  la  fonction $φ:x\mapsto  x^2-\frac{x^4}{2}$ de  la
  variable $x$ et en déduire le tracé de la ligne de niveau, ensemble des points
  $(x,y)$ tels que $V(x,y)=0$. Pour cela on cherchera à exprimer $y$ en fonction
  de $x$, en précisant le domainde de définition.
\item On considère la solution $(x,y)$ de  (1) qui obéit à la condition initiale
  $x(0)=0,\ y(0)=\frac{1}{\sqrt{2}}$. Préciser $V(x,y)$. Exprimer ensuite $y$ en
  fonction de $x$.
\item Déterminer  une primitive de  $x\mapsto \frac{1}{x^2-1}$ sur  $]-1,1[$. En
  déduire la solution  $(x,y)$ définie à la question  précédente. Tracer ensuite
  les graphes de $x$ et $y$.
  \end{enumerate}
  
\end{recherche}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% 
% E X E R C I C E S
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

 \modeexercice


\section{Petits calculs}


\begin{exercice}
Calculez les dérivées partielles d'ordre 1 de  la fonction 
\[f(x,y)=\ln\big(x(y-x)\big)\]



\end{exercice}



\begin{exercice}
Soit        $f~:~(x,y)\mapsto         \dfrac{1}{\sqrt{x^2+y^2}}$.        Montrez
que \[x\dfrac{\partial f}{\partial x}+y\dfrac{\partial f}{\partial y}=-f\]
\end{exercice}


\begin{exercice}
Soit R la résistance équivalente aux deux résistances $x$ et $y$ montées en parallèle.

Montrez que $R=\dfrac{x\cdot y}{x+y}$

Calculez la différentielle totale $\d R$.
\end{exercice}

\begin{exercice}[Loi des gaz parfaits]
  
Des gaz ayant certaines propriétés  plus tard sont
appelés \textit{gaz parfaits}. Ces gaz obéissent à une loi assez
remarquable $$PV=nRT$$ où où $P$, $V$ et $T$ représentent
respectivement la pression, le volume et la température du gaz . Le
nombre $n$ représente la quantité de matière exprimée en moles et
$R$ est la constante des gaz parfaits et vaut 8,3
$\text{J.K}^{-1}.\text{mol}^{-1}$.
\'Etudiez les courbes isochores (à volume constant), isobares (à
pression constante) et isothermes (à température constante)

\end{exercice}


\begin{exercice}
  
Calculer les d{\'e}riv{\'e}es partielles au premier ordre des fonctions 
suivantes :
\begin{enumerate}
\item
$f~:~(x,y)\mapsto x^{y^2}$.
\item
$f~:~(x,y)\mapsto\ln\displaystyle{
\frac{\sqrt{x^2+y^2}-x}{\sqrt{x^2+y^2}+x}}$.
\item
$f~:~(x,y)\mapsto\operatorname{arctan}\sqrt{\displaystyle{
\dfrac{x^2-y^2}{x^2+y^2}}}$.
\item
$f~:~(x,y,z)\mapsto\E^{x/y}+\E^{z/y}$.
\item $T~:~(P,V)\mapsto (\dfrac{1}{R}\pa{P+\dfrac{a}{V^2}}\pa{V-b}$ (Loi de Van der Walls)
\end{enumerate}
\end{exercice}


\begin{exercice}
  

  Démontrer que 
$f~:~(x,y)\mapsto\ln(x^2+y^2)$ v{\'e}rifie l'{\'e}quation de Laplace
$\dfrac{\partial^2 f}{\partial x^2}+\dfrac{\partial^2 f}{\partial y^2}=0$.

\end{exercice}

% \section{}


% \modenormal
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "Poly_f2vars21"
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
