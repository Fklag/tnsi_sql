{{ chapitre(2, "Ensembles, applications - Exercices", 24)}}

### Ensembles


!!! {{ exercice()}}

    === "Énoncé"

        Écrire par compréhension :
		
		- $\bigl\{\dots,-64,-27,-8,-1,0-1,8,27,64,\dots\bigr\}$
		
		- $\bigl\{1,5,9,13,17,21,25,\dots\bigr\}$

	=== "Indications"

         :/
		 
		 

!!! {{ exercice()}}

    === "Énoncé"

	     $A=\bigl\{n\inℤ\mid n^2 {\rm \ est\ impair}\bigr\}$ et
	     $B=\bigl\{n\inℕ\mid\exists p\inℤ, \, n=2p\bigr\}$. 

	     Identifier $A\cap B$ et $A\cup B$.
  

	=== "Indications"

         :-*


!!! {{ exercice()}}

    === "Énoncé"

          Comparer $\emptyset$, $\bigl\{\emptyset\bigr\}$, $\Bigl\{\bigl\{\emptyset\bigr\}\Bigr\}$

	=== "Indications"

         $\emptyset$


!!! {{ exercice()}}

    === "Énoncé"

        Parmi les ensembles suivants, quels sont ceux qui sont égaux ?

		-  $A=\bigl\{x\mid x\in {\mathbb R} \text{ et } x^2-4x+3=0\bigr\}$
		
		-  $B=\bigl\{x\mid x\in{\mathbb R} \text{ et } x^2-3x+2=0\bigr\}$
		
		-  $C=\bigl\{x\mid x\in{\mathbb N} \text{ et } x<3 \bigr\}$
		
		- $D=\bigl\{x\mid x\in{\mathbb N} \text{ et } x<5 \text{ et }x
		   {\rm \ est\ impair }\bigr\}$
		
		-  $E=\bigl\{1,2\bigr\}$
		
		-  $F=\bigl\{1,2,1\bigr\}$
		
		-  $G=\bigl\{3,1\bigr\}$
		
		-  $H=\bigl\{1,1,3\bigr\}$

	=== "Indications"

         

!!! {{ exercice()}}

    === "Énoncé"

        $E=\left\{ 0,1,2,3,4,5,6\right\}  .$ Définir en  extension (c'est-à-dire
        lister tous les éléments) les ensembles suivants : 
		
		- $A_{1}=\left\{ x\in \mathbb{N}\mid x^{2}\in E\right\} $
		
		- $A_{2}=\left\{ x\in \mathbb{R}\mid x^{2}\in E\right\} $
		
		- $A_{3}=\left\{ x\in E\mid x^{2}\in E\right\} $
		
		- $A_{4}=\left\{ x\in E\mid \sqrt{x}\in E\right\} $
		
		- $A_{5}=\left\{ x\in E\mid 2x\in E\right\} $
		
		-  $A_{6}=\left\{ x\in E\mid \frac{x}{2}\in E\right\} $

	 

	=== "Indications"

!!! {{ exercice()}}

    === "Énoncé"

        
        $E=\left\{ 0,1,2,3,4\right\} .$ Compléter, lorsque c'est possible, par
        un des symboles (il peut y avoir plusieurs solutions, mais on s'obligera 
        à choisir celle qui donne le plus de renseignements): 
        
		$$
		\in     ,\ni    ,\subseteq     ,\supseteq    ,=,\neq     ,\varsubsetneq,
		\not\subseteq,\cdots
		$$
		
		- $2\cdots E,$ 
		
		-  $\left\{ 2,3\right\} \cdots E,$ 
		
		-  $\left\{ 2\right\} \cdots E$,
		
		-  $\left\{ 2,3,4\right\} \cdots \left\{ 4,3,2\right\} ,$ 
		
		-  $\left\{2,3,4\right\} \cdots \left\{ 4,3,0\right\} $,
		
		-  $\{\} \cdots E,$ 
		
		-  $E\cdots E,$ 
		
		-  $E\cdots \left\{ E\right\} ,$ 
		
		-  $\{\}  \cdots \left\{ E\right\} $,
		
		-  $E\cdots \left\{ 0,1,2,3,\ldots ,10\right\}$,
		
		-   $\left\{0,1,2,3,4,5\right\} \cdots E$

	=== "Indications"

         

!!! {{ exercice()}}

    === "Énoncé"

	     Trouvez des analogies et des  différences entre $\subseteq$ définie sur
	     les ensembles et $\leqslant $ définie sur les réels.

	=== "Indications"

         

!!! {{ exercice()}}

    === "Énoncé"

	    Démontrez les propriétés de la section [dualité](../../COURS/2_sets/#dualite).

	=== "Indications"

         
!!! {{ exercice()}}

    === "Énoncé"

        Simplifiez au maximum les égalités suivantes :
		
		- $(A\cap B)\cup(A\cap \bar{B})$
		
		- $(A \cap \bar{B})\cup(\bar{A}\cap B)\cup(A\cap B)$

	=== "Indications"

         
!!! {{ exercice()}}

    === "Énoncé"

        On définit un opérateur $\uparrow$ sur les ensembles de la manière suivante :

	    $$
		A \uparrow B = \bar{(A \cap B)}
		$$

	    Démontrer les égalités suivantes :
		
		- $A \uparrow A = \bar{A}$;
		
		- $(A \uparrow A) \uparrow (B \uparrow B) =A \cup B$;
		
		- $(A \uparrow B) \uparrow (A \uparrow B) = A \ ???\ B$.
		
		- l'opérateur $\uparrow$ est-il [associatif](https://fr.wikipedia.org/wiki/Associativit%C3%A9) ?

	=== "Indications"

         


!!! {{ exercice()}}

    === "Énoncé"

        1.  Démontrer par  récurrence  que  si $E$  possède  $n$ éléments  alors
           ${\mathscr P} (E)$ possède  $2^{n}$ éléments.  En anglais, ${\mathscr
           P} (E)$ s'appelle \textit{power set of E}. 
		
		2.    Combien    d'éléments   ${\mathscr   P}   \left(    {\mathscr   P}
		     \left(  {\mathscr P}\left(  \left\{  a,b,c\right\} \right)  \right)
		     \right) $ contient-il ? 
	    
		3.  Combien d'éléments a ${\mathscr P} ({\mathscr P} ({\mathscr P}
		    ({\mathscr P} ({\mathscr P} (\emptyset)))))$?  Combien y a-t-il d'atomes dans l'Univers?...

	=== "Indications"

         

!!! {{ exercice()}}

    === "Énoncé"

        1.  Soit $A=\bigl\{0,1\bigr\}$.  Déterminer $\left(A^2\setminus \bigl\{(0,0)\bigr\}\right)\otimes A$.
	   
	    2. Soit $A=\bigl\{3,5,7\bigr\}$ et $B=\bigl\{a,b\bigr\}$. Déterminer $A^2$, $B^2$, $A\otimes B$,
          $B\otimes A$, $B^2\otimes A$.

	=== "Indications"

         

### Fonctions / Applications


!!! {{ exercice()}}

    === "Énoncé"

         Soit  $f:E\rightarrow  F$.  Traduire  en quantificateurs  la  phrase  :
         « $f$ n'est pas surjective ». 

	=== "Indications"

         
		 
!!! {{ exercice()}}

    === "Énoncé"

         Donner la représentation sagittale  d'une fonction totale de l'ensemble
         fini $E$ dans l'ensemble fini $F$ 

         - non injective et non surjective 
		 
		 - non injective mais surjective 
		 
		 - injective mais pas surjective
		 
	=== "Indications"

!!! {{ exercice()}}

    === "Énoncé"

         Soit $ f : ℝ \longrightarrow [0,+\infty[$ la fonction carré.
		 
		 Étudier l'injectivité, la surjectivité et la bijectivité de $f$ 

	=== "Indications"

         



!!! {{ exercice()}}

    === "Énoncé"

         On considère l'application

        $$
         f\colon \begin{array}{rll} {\mathbb R}^2& \to & {\mathbb R}^2\\
          (x,y) & \mapsto & (2x-3y,2xy)
         \end{array}
        $$

	     - Déterminer $f^{-1}(\{(0,0)\})$.
	
	     - L'application $f$ est-elle injective ?
         
		 - Déterminer $f^{-1}(\{(3,-1)\})$.
	
		 - L'application $f$ est-elle surjective?
	
          - Démontrer que $f(ℝ^2)=\bigl\{(a,b)\inℝ^2\mid a^2+12b\geqslant 0\bigr\}$.

	=== "Indications"

        

!!! {{ exercice()}}

    === "Énoncé"

        $f$  est une  fonction totale  de $E$  dans $F,$  $A$ et  $B$ sont  deux
        parties de $E$ et $A^{\prime }$ et $B^{\prime }$ sont deux parties de $F.$ Démontrer :

         - $A\subseteq B⟹ f(A)\subseteq f(B)$

         - $f(A\cup B)=f(A)\cup f(B)$

         - $f(A\cap B)\subseteq f(A)\cap f(B),$ donner un exemple où il n'y a pas égalité.

         - $A\subseteq f^{-1}\left( f(A)\right) $

         - $f^{-1}(A^{\prime }\cap B^{\prime })=f^{-1}(A^{\prime })\cap f^{-1}(B^{\prime })$

         - $f\left( f^{-1}(B^{\prime })\right) \subseteq B^{\prime }$

         - $A^{\prime }\subseteq B^{\prime }⟹ f^{-1}(A^{\prime})\subseteq f^{-1}(B^{\prime })$

         - $f^{-1}(A^{\prime }\cap B^{\prime })=f^{-1}(A^{\prime })\cap f^{-1}(B^{\prime })$

	=== "Indications"

         

