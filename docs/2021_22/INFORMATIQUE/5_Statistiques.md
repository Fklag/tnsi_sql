{{ chapitre(5, "Statistiques", 0)}}


L'objectif est de rassembler les fonctions permettant le calcul des principales caractéristiques de séries statistiques simples et doubles...
  
On rappelle qu'il s'agit de programmer des fonctions par ailleurs bien connues de la bibliothèque numpy, à savoir :
`np.mean(X)`,`np.var(X)`,`np.std(X)`,`np.percentile(X)`,`np.cov(X,Y)`,`np.corrcoef(X,Y)` ou encore `np.polyfit(X,Y,1)`

## Importation préalable des outils Python nécessaires :


```python
import numpy as np
import matplotlib.pyplot as plt
from typing import List
```

## I. Un exemple élémentaire.

On suppose avoir réalisé dix lancers successifs d'un dé dont les résultats sont reproduits ci-dessous sous forme de liste.


```python
X = [1,2,5,1,6,4,6,3,1,2]
```

**Exercice I.1 :**  
1. Déterminer le nombre de 'six'.


```python
?????
```




    2



2. Déterminer le tableau `Teff` des effectifs et l'afficher :


```python
Teff = ????
print('Teff = ',Teff)
```

    Teff =  [3, 2, 1, 1, 1, 2]


Représenter l'histogramme des effectifs en regardant l'aide de `plt.bars`


```python
bars = plt.bar(????, ????, width=0.8, color='lightslategrey')
bars[5].set_color('r')
```


    
![png](output_12_0.png)
    

Idem avec une couronne (cf `plt.pie`)


```python
size = 0.5
tabE = [0, 0, 0, 0, 0, 0.2]
tabL = ['un', 'deux', 'trois', 'quatre', 'cinq', 'six']
c = plt.pie(???, explode = tabE, wedgeprops=dict(width=size, edgecolor='w'),  labels = tabL)
```


    
![png](output_13_0.png)
    


3. Déterminer le tableau `Effcum` des effectifs cumulés :


```python
Effcum = ?????
print("les effectifs cumulés sont : ",Effcum)
```

    les effectifs cumulés sont :  [3, 5, 6, 7, 8, 10]


4. Représenter l'histogramme des effectifs cumulés :


```python
plt.bar(???, ????, align='edge', width=1)
```




    <BarContainer object of 6 artists>




    
![png](output_17_1.png)
    


5.  Utiliser  la   bibliothèque  numpy  pour  obtenir   directement,  au  choix,
   l'histogramme des effectifs, des effectifs relatifs, des effectifs cumulés : 


```python
H = plt.hist(?????)
print(H[0])
print(H[1])
```

    [0.3 0.2 0.1 0.1 0.1 0.2]
    [1 2 3 4 5 6 7]



    
![png](output_19_1.png)
    


## 2. Caractéristiques de position.

**Exercice 2.1 :** Ecriture des fonctions calculant la médiane et la moyenne.  
1. Compléter la ligne  de code ci-dessous pour trier la  liste X (comment dit-on
   *trié* en anglais?)


```python
Xtriee = ????
print(X)
print(Xtriee)
```

    [1, 2, 5, 1, 6, 4, 6, 3, 1, 2]
    [1, 1, 1, 2, 2, 3, 4, 5, 6, 6]


2. Compléter la fonction suivante afin qu'elle retourne la médiane d'une liste `LX` 


```python
def mediane(LX):
    trie = ???
    n = len(trie)
    if n%2 ????: 
        return ????
    else : 
        return ?????
        
print ('la mediane de X vaut : ',mediane(X) )
```

    la mediane de X vaut :  2.5


Pour le premier quartile, on pourra faire :


```python
def premierQuartile(LX):
    trie = ???
    n = len(LX)
    return ????
```


```python
print('le 1er quartile de X est :',premierQuartile(X))
```

    le 1er quartile de X est : 1


Qu'on comparera avec le résultat fourni par la fonction Python `np.percentile()` :


```python
print(np.percentile(X,50))
```

    2.5


*Remarque :* On pourra synthétiser l'ensemble de ces résultats grâce à une "boîte à moustaches" :


```python
B = plt.boxplot(X)
```


    
![png](output_31_0.png)
    


3. Compléter la fonction `moyenne(LX)` de paramètre d'entrée une liste `LX`, afin qu'elle retourne sa moyenne.


```python
def moyenne1(LX):
    S = ???
    n = ???
    for ??? in ???:
        S ????
    return ?????
```

*Remarque :* Si on nous autorise la fonction `sum`, il est évidemment possible d'écrire :


```python
def moyenne2(LX):
    return sum(LX)/len(LX)
```


```python
print(moyenne1(X));print(moyenne2(X));print(np.mean(X))
```

    3.1
    3.1
    3.1


*Remarque :* On peut aussi faire apparaître la moyenne sur la "boîte à moustache" en précisant le booléen `showmeans`


```python
B2 = plt.boxplot(X,showmeans='true')
```


    
![png](output_38_0.png)
    


## 3. Caractéristique de dispersion.


```python
def variance1(LX):
    ????
```


```python
print("la variance de X vaut :",variance1(X))
print(np.var(X))
```

    la variance de X vaut : 3.6899999999999995
    3.69


## 4. Les séries multivariées.

A titre d'exemple, on prendra par la suite les séries `X` et `Y` suivantes :


```python
X2 = [1.8,2.3,3.4,3.9,5.6,6.1,6.5,8.3,8.9]
Y2 = [0.9,2.1,2.6,4.1,6.2,5.8,7.1,8.1,7.9]
```


```python
def covariance1(LX,LY):
    ????
```


```python
covariance1(X2, Y2)
```




    5.789999999999999




```python
def variance2(X):
    return covariance1(X,X)
```


```python
variance2(X2)
```




    5.65111111111111




```python
#print(covariance1(X2,Y2))#;
print(np.cov(X2,Y2,bias=1))
```

    [[5.65111111 5.79      ]
     [5.79       6.25506173]]



```python
def coeff_de_corr1(X,Y):
    # directement accessible avec "np.corrcoef(x,y)"
    ????
```


```python
print(coeff_de_corr1(X2,Y2));print(np.corrcoef(X2,Y2))
```

    0.9738584582190766
    [[1.         0.97385846]
     [0.97385846 1.        ]]



```python
def regression1(LX,LY):
    # calcul des coefficients et représentation graphique.
    a = ???
    b = ???
    Yregr = [????]
    plt.figure()
    plt.plot(LX, LY, 'o', LX, Yregr, '-')
    plt.show()
    return a, b
```


```python
a1,b1 = np.polyfit(X2, Y2, 1)
```


```python
a1,b1
```




    (1.0245772709398346, -0.35002403110936053)




```python
a,b = regression1(X2, Y2)
```


    
![png](output_55_0.png)
    



```python
a,b
```




    (1.0245772709398349, -0.3500240311093634)




```python

```
