## Minimum d'une liste

def ind_mini(xs: list, debut):
    """
    Renvoie l'indice de 1ère apparition du mini d'une liste
    """
    mini = xs[debut]
    i_min = debut
    for i in range(debut, len(xs)):
        if xs[i] < mini :
            i_min = i
            mini = xs[i]
    return i_min
    
def tri_selec(ma_liste: list) -> list:
    xs = ma_liste[:]
    for debut in range(len(xs) - 1) :
        i_min = ind_mini(xs, debut)
        xs[debut], xs[i_min] = xs[i_min], xs[debut]
        print(xs)
    return xs
    