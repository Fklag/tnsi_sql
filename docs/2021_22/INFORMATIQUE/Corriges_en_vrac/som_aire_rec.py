def aire_bleue(f, a, b, n):
    som = 0
    pas = (b - a) / n
    x = a
    # ici x vaut x_0
    for k in range(0, n):
        som += f(x) # j'ajoute la hauteur f(x_k)
        x += pas # j'avance d'un pas : x vaut x_(k + 1)
    # som vaut f(x_0) + ... + f(x_(n-1))
    # x vaut x_n
    return som*pas

def aire_rouge(f, a, b, n):
    som = 0
    pas = (b - a) / n
    x = a + pas
    # ici x vaut x_1
    for _ in range(n): # on va répéter n fois la même action
        som += f(x) # j'ajoute la hauteur f(x_k)
        x += pas # j'avance d'un pas : x vaut x_(k + 1)
    # som vaut f(x_1) + ... + f(x_n)
    # x vaut x_(n +1)
    return som*pas