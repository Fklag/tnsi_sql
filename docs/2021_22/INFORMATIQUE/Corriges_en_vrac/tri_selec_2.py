def ind_mini(xs: list, debut: int) -> int:
    """
    Renvoie l'indice de 1ère apparition du mini
    d'une tranche de la liste xs qui commence à l'indice debut
    """
    mini = xs[debut] # on commence à lire à l'indice debut
    i_min = debut
    for i in range(debut, len(xs)):
        if xs[i] < mini:
            mini = xs[i]
            i_min = i
    return i_min
    
def tri_selec(ma_liste: list) -> list:
    xs = ma_liste[:] # copie indépendante
    for debut in range(len(xs) - 1) : # les débuts montent
        i_min = ind_mini(xs, debut) # l'indice du  mini de la tranche
        xs[debut], xs[i_min] = xs[i_min], xs[debut] # on le met au début de la tranche
    return xs
    
# TODO tout faire dans une seule fonction