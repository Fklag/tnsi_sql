mot = input("Bonjour jeune Padawan, quel est ton mot ? ")

def pal(mot: str) -> bool:
    i = 0
    long = len(mot)
    while mot[i] == mot[-i - 1] and i < long // 2 : # tant qu'il y a symétrie et qu'on n'a pas dépassé la moitié du mot
        i += 1 # on avance 
    return i == long//2 

if pal(mot):
    print(f"{mot} est un palindrome")
else:
    print(f"{mot} n'est pas un palindrome")

# tom = ""
# for lettre in mot:
#     tom = lettre + tom
# print(f"Endroit : {mot}")
# print(f"Envers  : {tom}")
# if mot == tom:
#     print("C'est un palindrome")