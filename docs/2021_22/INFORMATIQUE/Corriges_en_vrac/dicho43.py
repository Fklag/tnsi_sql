from math import log

def f42(x):
    return log(x) - log(x + 1) + 1/x

def dicho(a, b, f, n):
    """
    Cherche une valeur approchée par excès de f(x) = 1
    entre a et b
    """
    c = 0.5*(a + b)
    if (b - a) < 10**(-n):
        return b
    elif f(c) > 1:
        return dicho(c, b, f,  n)
    else:
        return dicho(a, c, f, n)
    
def dichow(a, b, f, n):
    deb = a
    fin = b
    while fin - deb > 10**(-n):
        c= 0.5*(deb + fin)
        if f(c) > 1:
            deb = c
        else:
            fin = c
    return fin
    