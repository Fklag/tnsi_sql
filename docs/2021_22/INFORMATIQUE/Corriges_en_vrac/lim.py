from sympy import pprint, limit, sqrt, sin, cos, pi, log, series, exp, tan
from sympy.abc import x
import matplotlib.pyplot as plt
import numpy as np

f = lambda x: tan(x)
pprint(series(f(x), x, 0, 6))
