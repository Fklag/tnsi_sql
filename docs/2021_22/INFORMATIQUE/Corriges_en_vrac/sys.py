import numpy as np
import matplotlib.pyplot as plt
from urllib.request import urlretrieve

url = "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/98/9843d072473afaffb2c910416993d7cb0845395c_full.jpg"
urlretrieve(url, filename="./kermit.jpg")
kermit = plt.imread("./kermit.jpg")










def gris1(im):
    h, w, _ = im.shape
    im_new = np.zeros(im.shape, dtype="uint8")
    for i in range(h):
        for j in range(w):
            g = np.sum(im[i, j])//3
            im_new[i, j] = np.array([g, g, g])
    return im_new

# La formule obscure
def lin(vs):
    return 1.055*vs**(1/24) - 0.055 if 0.0031308 <vs<=1 else 12.92*vs

# luminance d'un pixel
def lumi_pix(pix):
    r, g, b = pix
    for vs in [r,g,b]:
        vs = int(255*lin(vs/255))
    g = int(0.2126*r + 0.7152*g+0.0722*b)
    return np.array([g, g, g], dtype='uint8')

# on renvoie la matrice de luminance de chaque pixel
def gris(im):
    return np.array([[lumi_pix(pix) for pix in ligne] for ligne in im])


_, imtab = plt.subplots(1, 3, figsize=(12, 12))

imtab[0].imshow(gris1(kermit))
imtab[1].imshow(kermit)
imtab[2].imshow(gris(kermit))