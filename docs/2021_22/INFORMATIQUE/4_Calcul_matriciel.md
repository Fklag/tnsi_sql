{{ chapitre(4, "Calcul matriciel", 0)}}


# Poly

Parcourir le Poly  et répondre à tous les problèmes  posés sur la représentation
des matrices en Python.


# Voir des matrices

## Préliminaire

Construisez  une fonction  `mat_pow(M: np.ndarray,  n: int)  -> np.ndarray`  qui
renvoie la puissance n-ième de la matrice M. (NB : cette fonction existe déjà dans
`numpy` et s'appelle `np.linalg.matrix_power`)


## Coloration

Explorer le code suivant :

```python
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import matplotlib.colors as colors

taille = 6 # taille de la matrice

Z = np.array([[1 if i < j else 0 for j in range(taille)] for i in range(taille)])

nf = 4
mats = [mat_pow(Z,i + 1) for i in range(nf)]
n = int(np.max(mats)) + 2

# Fabrication d'une barre de couleurs adaptée
cols = []
for i in range(n):
    cols.append(tuple([np.random.rand() for _ in range(4)]))
mes_couleurs = colors.ListedColormap(cols) 
ma_norme = colors.BoundaryNorm(range(len(cols)), mes_couleurs.N, clip=True)
# Fin de la fabrication de la colormap

for i in range(nf):
    plt.clf()
    im = plt.imshow(mat_pow(Z,i + 1), cmap=mes_couleurs, norm=ma_norme)
    cb = plt.colorbar()
    plt.show()
```

Testez-le avec d'autres  matrices de l'exercice sur les  matrices nilpotentes ou
sur les calculs de puissances.

Créer une fonction `affiche_mat` prenant une matrice en argument et qui l'affiche en couleurs
puis une autre fonction  qui prend une matrice et un entier  `n` en arguments et
qui affiche ses `n` premières puissances.


## Art

Afficher  des   croix,  losanges,  damiers   etc.  'à  l'aide  de   la  fonction
`affiche_mat`.

## Photoshop

### Exploration

```python
cb.remove()

from scipy import misc
mystere = misc.face()
```

Qu'est-ce que `mystere` ? 

### Manipulation

Afficher un raton-laveur:

- en négatif
- en niveaux de gris
- la tête en bas
- la patte vers la gauche
- juste sa tête


Des éléments de réponses sont [ICI](./image_mat.md)
