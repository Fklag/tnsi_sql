{{ chapitre(3, "Récursion", 0)}}


# Recursion is everywhere


!!! quote


	This is the horse and the hound and the horn

	That belonged to the farmer sowing his corn

	That kept the rooster that crowed in the morn

	That woke the judge all shaven and shorn

	That married the man all tattered and torn

	That kissed the maiden all forlorn

	That milked the cow with the crumpled horn

	That tossed the dog that worried the cat

	That killed the rat that ate the malt

	That lay in the house that Jack built.
	

![<img src="./IMG/inception.png" width="600"/>](./IMG/inception.png)

![<img src="./IMG/umaguma.png" width="600"/>](./IMG/umaguma.png)

![vache](./IMG/vache.gif)

![k2000](./IMG/k2000.gif)

# Récursion / Récursivité


![fixing_problems](./IMG/fixing_problems.webp)

emprunté à l'anglais dans les années 1920,  le mot anglais tirant son origine du
latin *recurrere* : courir en arrière.

## Exemple basique 

```python
def f1(n: int) -> int:
    assert n > 0, "n doit être positif"
    return 1 if n == 1 else n*f1(n - 1)
```

Voilà comment cela se passe:

```python
f1(5)
5 * f1(4)
5 * (4 * f1(3))
5 * (4 * (3 * f1(2)))
5 * (4 * (3 * (2 * f1(1))))
5 * (4 * (3 * (2 * 1)))
5 * (4 * (3 * 2))
5 * (4 * 6)
5 * 24
120
```


![stack](./IMG/stack.webp)

# Quelques exercices

- Bing : Bing is not Google
- GNU : GNU's Not UNIX
- PNG : PNG's Not GIF
- LAME : Lame Ain't an MP3 Encoder

![dilbert](./IMG/dilbert.gif)


## Quizz

1. On considère la fonction suivante :
   ~~~python
   def f(n):
       if n == 0:
	       return 1
       else:
	       return 2 * f(n - 1)
   ~~~
   Comment est évalué `f(8)` ?

    1. Ce calcul provoque une erreur.
    2. Cette expression vaut 14.
    3. Cette expression vaut 256.
    4. Cette expression vaut 2.

2. La pile d'exécution est :

    1. utilisée lors des tests `if`.
    2. utilisée lors des appels récursifs.
    3. composée d'éléments chimiques et soudée sur la carte mère.
    
3. On considère la fonction suivante :
   ~~~python
   def inverse(lst):
       return inverse(lst[1:]) + lst[0]
   ~~~
   Cette fonction :

    1. renvoie l'image miroir d'une liste.
    2. calcule la somme des éléments de la liste
    3. compte le nombre d'éléments de la liste
    4. renvoie une erreur (et dans ce cas la corriger).

4. Le programme suivant renvoie une erreur.
   ~~~python
   def liste_entier_decr(n):
       if n == 0:
	       return 0
       return [n] + liste_entier_decr(n - 1)
   ~~~

    1. Vrai (et dans ce cas la corriger)
    2. Faux



## Somme épisode 1

Déterminer une fonction récursive qui calcule la somme des entiers de 1 à un certain entier naturel `n` donné en argument. 

## Somme épisode 2

Déterminer une fonction récursive qui calcule la somme des éléments d'une liste donnée en argument. 

## Longueur

Déterminer une fonction récursive qui renvoie la longueur d'une liste.

## Compteur

Déterminez  une  fonction  récursive  qui compte  le  nombre  d'occurences  d'un
caractère dans une chaîne, tous deux donnés en paramètre.


## Puissance entière 

Déterminez une fonction récursive qui calcule $a^n$ avec $n∈ℕ$


## Palindrome

Déterminez une fonction récursive qui vérifie si un mot est un palindrome. 

## Max

Déterminer une fonction récursive qui détermine le maximum d'une liste de nombres. On n'utilisera bien sûr pas le `max` de Python. On pourra commencer par créer une fonction `max_de_2(x1, x2)` qui renvoie le plus grand entre les nombres `x1` et `x2`.

## PGCD

Le plus grand commun diviseur de deux nombres, notamment utile pour écrire une fraction sous forme irréductible, peut être  déterminé efficacement à l'aide de l'algorithme d'Euclide.

Celui-ci affirme que le pgcd de `a` et de `b`, `b` étant un entier non nul, est le même que le pgcd de `b` et de `c`, avec `c` le reste de la division euclidienne de `a` par `b`. Le pgcd de `a` et de 0 étant `a`. 

1. Écrire une fonction récursive calculant le pgcd de `a` et de `b`.

2. En utilisant la fonction précédente écrire une fonction qui prend en entrée deux nombres entiers naturels non nuls `a` et b représentant une fraction $\frac{a}{b}$ et qui renvoie un couple d'entiers c et d tels que $\frac{c}{d}$ est l'écriture sous forme irréductible de $\frac{a}{b}$.

## Générer récursivement une liste

Écrire une fonction récursive d'argument `n` qui génère la liste

```Python
[n, n-1, ... , 1, 0]
```

## Dénombrement


1. Déterminer une fonction récursive qui calcule $A_n^p$.
1.  Déterminer plusieurs  fonctions  récursives qui  calculent $\binom{n}{p}$  :
   essayer à chaque  fois de trouver d'améliorer l'efficacité  de votre fonction
   en terme de calculs effectués par la machine.


## Quelques solutions


```python
## utilitaires

def tete(xs):
    return xs[0]

def queue(xs):
    return xs[1:]

## SOMME1

def somme1(n: int) -> int:
    """
    Calcule la somme des entiers de 1 à n
    """
    assert n >= 1, "n doit être supérieur à 1"
    if n == 1:
        return 1
    else:
        return n + somme1(n - 1)

def somme1_v2(n: int) -> int:
    assert n >= 1, "n doit être supérieur à 1"
    return 1 if n == 1 else n + somme1(n - 1)

def somme1_v3(n: int, s = 1) -> int:
    assert n >= 1, "n doit être supérieur à 1"
    return s if n == 1 else somme1_v3(n - 1, s + n)



def test_somme1(n: int) -> bool:
    return somme1(n) == n*(n + 1)/2


## SOMME2

def somme2(xs: list):
    assert xs != [], "la liste doit être non vide"
    if len(xs) == 1:
        return xs[0]
    else:
        return xs[0] + somme2(xs[1:])

def somme2_v2(xs: list):
    assert xs != [], "la liste doit être non vide"
    if len(xs) == 1:
        return tete(xs)
    else:
        return tete(xs) + somme2(queue(xs))

    

## LONGUEUR

def long(xs: list) -> int:
    return 0 if xs == [] else 1 + xs[1:]

def long_v2(xs: list, lg = 0) -> int:
    return lg if xs == [] else long_v2(xs[1:], lg + 1)

def long_v3(xs: list, lg = 0) -> int:
    return lg if xs == [] else long_v2(queue(xs), lg + 1)


## Compteur de lettres

def compte_car(mot: str, lettre: str) -> int:
    if mot == "":
        return 0
    elif lettre == mot[0]:
        return 1 + compte_car(mot[1:], lettre)
    else:
        return compte_car(mot[1:], lettre)
        
def compte_car(mot: str, lettre: str) -> int:
    if mot == "":
        return 0
    elif lettre == tete(mot):
        return 1 + compte_car(queue(mot), lettre)
    else:
        return compte_car(queue(mot), lettre)
```
