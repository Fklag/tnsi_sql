{{ chapitre(8, "Tris : la découverte", 0)}}


# Les tris

## Tri à bulle

![tribulle](./IMG/bubble_sort.gif "Tri à bulle")

```python
def tri_bulle(xs: list):
    for i_stop in range(len(xs) - 1, -1, -1): # de droite à gauche je place les maxis
        for i in range(i_stop): # le caillou actif qui se compare à son suivant
            if xs[i] > xs[i + 1]: # s'il est plus grand que son suivant
                xs[i], xs[i + 1] = xs[i + 1], xs[i] # on les échange
    return xs
```

ou dans l'autre sens :


![tribulle2](./IMG/bubble_sort2.gif "Tri à bulle")


```python
def tri_bulle_2(xs: list):
    for i_debut in range(0, len(xs) - 1): # de gauche à droite je place les minis
        for i in range(len(xs) - 2, i_debut - 1, -1): # le caillou actif qui se compare à son suivant
            if xs[i] > xs[i + 1]: # s'il est plus grand que son suivant
                xs[i], xs[i + 1] = xs[i + 1], xs[i] # on les échange
    return xs
```


## Tri par insertion

![triinser](./IMG/insertion_sort.gif "Tri par insertion\")

```python
def insere(elmt, cs: list) -> list:
    cs.append(elmt) # on insère elmt par la doite dans liste_triee
    n = len(cs)
    i = n - 1 # indice de départ 
    while cs[i] < cs[i - 1] and i > 0: # tant que le ieme est mal placé
        cs[i - 1], cs[i] = cs[i], cs[i - 1] # on échange 
        i -= 1 # on recule d'un cran
    return cs

def tri_insere(Pioche: list) -> list:
    Main = []
    for carte in Pioche:
        Main = insere(carte, Main) # on insère les cartes une par une
    return Main

def tri_insere_2(jeu: list) -> list:
    for i in range(len(jeu)):
        jeu[:i + 1] = insere(jeu[i], jeu[:i]) # on insère les cartes dans le début du jeu
    return jeu
```

Étudiez en particulier la différence entre les deux versions du tri.


## Tri par selection

![triselec](./IMG/selection-sort.gif "Tri par sélection\")


```python
def ind_mini(xs: list, debut: int) -> int:
    """
    Renvoie l'indice de 1ère apparition du mini
    d'une tranche de la liste xs qui commence à l'indice debut
    """
    mini = xs[debut] # on commence à lire à l'indice debut
    i_min = debut
    for i in range(...):
        if ... :
            ...
            ...
    return ...
    
def tri_selec(ma_liste: list) -> list:
    xs = ma_liste[:] # copie indépendante
    for debut in range(...) : # les débuts montent
        i_min = ... # l'indice du  mini de la tranche
        ..., ... = ... # on le met au début de la tranche
    return xs
    
# TODO tout faire dans une seule fonction
```


# La visualisation


Expliquer ce qui est fait ici et utilisez ces fonctions

```python
import matplotlib.pyplot as plt
import numpy as np
from np.random import choice
from time import perf_counter


def test(tri):
    """ teste 20 fois le tri donné en argument sur des listes de taille 1000"""
    for _ in range(20):
        t = liste(1000)
        assert tri(t) == sorted(t)
    print(f"{tri} est OK après 20 tests")
    
    
def liste(n):
    """Liste aléatoire de n entiers entre 0 et 10*n"""
    return list(choice(range(10*n), n))

def temps(tri, n):
    p = liste(n)
    debut = perf_counter()
    tri(p)
    return perf_counter() - debut

def liste_temps(tri):
    n = 10
    xs, ts = [], []
    while n < 4000:
        t = temps(tri, n)
        ts.append(t)
        xs.append(n)
        n = int(n*1.2)
    return xs, ts

def trace(liste_tris):
    plt.clf()
    for tri in liste_tris:
        x, y = liste_temps(tri)
        plt.plot(x, y, label = str(tri)[9:-19])
    plt.legend()
    plt.show()
```


# Tri  par comptage

## Principe

Le tri par comptage sert à trier des tableaux de nombres entiers. Le principe
est simple :

- On  dispose d'un tableau  `tab` d'entiers naturels  que l'on veut  trier, avec
  d'éventuelles répétitions, par exemple :
  
  ```python
  tab = [5, 4, 1, 5, 2, 12, 2, 4, 0, 5, 5, 2]
  ```
  
- On  crée un tableau  représentant l'*histogramme*  de cette série  de nombres,
  comme nous l'avons déjà vu dans notre premier TP.
  
  Par exemple, ici, cela donnera :
  
  ```python
  histo = [1, 1, 3, 0, 2, 4, 0, 0, 0, 0, 0, 0, 1]
  ```
  
-  On crée  une liste  vide  `liste-triée` que  l'on  remplit petit  à petit  en
  parcourant `histo`.  On va par  exemple y rajouter un  `0`, puis un  `1`, puis
  trois `2`,  puis aucun  `3` puis  deux `4` puis  quatre `5`  et enfin  un `12`
  c'est-à-dire:
  
  ```python
  liste_triée = [0, 1, 2, 2, 2, 4, 4, 5, 5, 5, 5, 12]
  ```

## Programmation

À  vous de  mettre  en œuvre  une  fonction qui  prendra  un tableau  d'entiers
naturels en paramètre  et renverra ce tableau trié dans  l'ordre croissant selon
ce principe.

```python
def tri_compte(entiers: list) -> list:
    m = max(...)
    compteurs = [0 for _ in range(...)] # Au départ, on met les compteurs à zéro
    # compte est l'histogramme de xs
    for entier in entiers:
        compteurs[...] += 1
    entiers_tries = [] # on va ajouter chaque entier autant de fois qu'il est présent
    for i in range(...):
        for occ in range(...):
            entiers_tries.append(...)
    return ...
```

## Complexité

Est-ce que ce tri est plus efficace que les tris précédemment étudiés ?

## Médiane

Comment adapter  ce tri à  la recherche  des quartiles d'une  série statistique
d'entiers ?




# Récupération, lecture et écriture de fichiers de données

## Tailles des joueurs de 2nde ligne de l'ASM depuis 1900.


On dispose  d'un [tableau](http://www.cybervulcans.net/modules/bd/stats_taille-poids.php?poste=4&Submit=Envoyer) donnant  la taille et la  masse des joueurs  ayant joué
2nde ligne à l'A.S. Montferrand depuis le début du XXe siècle.

![merle](./IMG/merle.webp "merle")

On peut récupérer ces données en utilisant la bibliothèque `pandas` qui est au
programme de BCPST et que nous utiliserons selon nos besoins ces prochains mois.

Il faudra commencer par l'installer !

```shell
pip install pandas
```

Puis nous allons l'utiliser pour extraire des tableaux mis en ligne. On commence par récupérer l'adresse de la page où se situent les données :

```python
import pandas 

url = "http://www.cybervulcans.net/modules/bd/stats_taille-poids.php?poste=4&Submit=Envoyer"
```

**URL** signifie  *Uniform Resource  Locator* (ou,  en français,  « localisateur
uniforme de ressource »). Une URL est simplement l'adresse d'une ressource en ligne.



On utilise ensuite la fonction `read_html` qui va extraire les tableaux présents
sur une page donnée. Comme ici il n'y  en a qu'un seul, on va extraire le numéro
`0`. On va se  servir de la ligne `1` comme en-tête :  c'est celle contenant les
titres des colonnes.

```python
tab = pandas.read_html(url, header=1)[0]
```

et on obtient:

```
             Nom Prénom                                     Poste       Né le  Taille  Poids
0       AMATOSERO Miles                               2ème ligne,  15/06/0202     201    125
1           PUECH Louis                   2ème ligne, 3ème ligne,  04/12/1894     181     83
2          COURTEZ René           Pilier, 2ème ligne, 3ème ligne,  00/00/1898     178     86
3       MARMAYOU Joseph  Pilier, 2ème ligne, 3ème ligne, Arrière,  02/10/1898     170     78
4           REMUS André                   2ème ligne, 3ème ligne,  00/00/1899     188     85
..                  ...                                       ...         ...     ...    ...
116      ITURRIA Arthur                   2ème ligne, 3ème ligne,  13/05/1994     198    107
117     ASTIER Corentin                               2ème ligne,  12/01/1995     198    117
118  VAN TONDER Jacobus                   2ème ligne, 3ème ligne,  03/03/1998     197    118
119       LANEN Thibaud                   2ème ligne, 3ème ligne,  01/04/1999     196    102
120    ANNANDALE Edward                               2ème ligne,  19/01/2001     200    116

[121 rows x 5 columns]
```

On va par exemple extraire la colonne des tailles :

```python
tailles = tab["Taille"]
```

et en  faire une liste  puisque nous avons construit  une fonction qui  trie les
listes avec le tri par comptage.

```python
ts = [int(taille) for taille in tailles]
```

`ts` est une liste contenant 121  entiers naturels correspondant aux tailles des
121 joueurs ayant été 2nd ligne au cours de l'histoire de l'ASM.


Il ne reste plus qu'à trouver la taille médiane et les autres quartiles à l'aide
du tri par comptage  puis de faire la même chose avec  les poids puis d'explorer
les autres postes.


## Génôme du SARS COVID 2

### Récupération du génôme


Vous pouvez retrouver le génôme du *Severe acute respiratory syndrome coronavirus
2          isolate          Wuhan-Hu-1*          en          suivant          [ce
lien](https://www.ncbi.nlm.nih.gov/projects/sviewer/?id=NC_045512&tracks=%5Bkey:sequence_track,name:Sequence,display_name:Sequence,id:STD649220238,annots:Sequence,ShowLabel:false,ColorGaps:false,shown:true,order:1%5D%5Bkey:gene_model_track,name:Genes,display_name:Genes,id:STD3194982005,annots:Unnamed,Options:ShowAllButGenes,CDSProductFeats:true,NtRuler:true,AaRuler:true,HighlightMode:2,ShowLabel:true,shown:true,order:9%5D&v=1:29903&c=null&select=null&slim=0).


Nous n'allons travailler que la suite des G,  A, C et T au format `txt` à l'aide
de Python.

Elle commence ainsi:

```
CTGCTTGTTGTCATCTCGCAAAGGCTCTCAATGACTTCAGTAACTCAGGTTCTGATGTTCTTTACCAACCACCACAAACCTCTATCACCTCAGCTGTTTTGCAGAGTGGTTTTAGAAAAATGGCATTCCCATCTGGTAAAGTTGAGGGTTGTATGGTACAAGTAACTTGTGGTACAACTACACTTAACGGTCTTTGGCTTGATGACGTAGTTTACTGT
```

Nous allons commencer par récupérer ce fichier qui est en ligne :

```python
from urllib.request import urlretrieve

url_genome = "https://edenmaths.gitlab.io/bcpst1/2021_22/INFORMATIQUE/SARSCOV2.txt"
urlretrieve(url_genome, filename="./genome.txt")
with open("./genome.txt", "r") as gen:
	covid = gen.read().rstrip()
```


La méthode  `read`  va  permettre  de   transformer  ce  texte  en  chaîne  de
caractères.  La  méthode  `rstrip`  permet  d'effacer  les  caractères  spéciaux
indésirables : passage à la ligne, tabulation, espaces à la fin, etc.

```
>>> covid

'CTGCTTGTTGTCATCTCGCAAAGGCTCTCAATGACTTCAGTAACTCAGGTTCTGATGTTCTTTACCAACCACCACAAACCTCTATCACCTCAGCTGTTTTGCAGAGTGGTTTTAGAAAAATGGCATTCCCATCTGGTAAAGTTGAGGGTTGTATGGTACAAGTAACTTGTGGTACAACTACACTTAACGGTCTTTGGCTTGATGACGTAGTTTACTGTCCAAGACATGTGATCTGCACCTCTGAAGACATGCTTAACCCTA...'
```

Nous voudrions classer les bases azotées selon leur fréquence à l'aide du tri
par comptage. Le problème est que cette fois les données ne sont pas des entiers
mais une des lettres `A`, `C`, `G` ou `T`.



### Découverte des dictionnaires Python


Nous  avions jusqu'à  maintenant  manipulé des  données à  l'aide  de liste  qui
étaient indexées par des  entiers : on y accède aux éléments  par leur rang dans
la liste.

Il  existe une  autre structure  de donnée  qui permet  de choisir  le type  des
indices qui peuvent notamment être des chaînes de caractères. Par exemple :

```python
In [1]: chaines = {'A': 5, 'C': 2, 'G': 24, 'T': 12}

In [2]: chaines['A']
Out[2]: 5

In [3]: bases = [cles for cles in chaines]

In [4]: bases
Out[4]: ['A', 'C', 'G', 'T']

In [5]: freqs = [chaines[cle] for cle in chaines]

In [6]: freqs
Out[6]: [5, 2, 24, 12]

In [7]: chaines['A'] += 10

In [8]: chaines
Out[8]: {'A': 15, 'C': 2, 'G': 24, 'T': 12}

In [9]: {base: 0 for base in 'ACGT'}
Out[9]: {'A': 0, 'C': 0, 'G': 0, 'T': 0}
```

Un dictionnaire  est donc une  structure mutable  associant des *valeurs*  à des
*clés*.  On  utilise des  accolades  comme  pour  les  ensembles car  c'est  une
structure par essence non ordonnée.


Déterminer une fonction qui va compter les bases azotées
dans un génôme et renvoyer un dictionnaire `base: fréquence`:

```python
In [1]: compte_bases(gs)
Out[1]: {'A': 2953, 'C': 1817, 'G': 1985, 'T': 3245}
```

Comment trier les bases selon leur fréquence ?

Déterminer un  dictionnaire des  fréquences des n-uplets  de bases.  Par exemple
pour les doublets:

```python
compte_nuplets(gs, 2)

{'CT': 694,
 'TG': 935,
 'GC': 396,
 'TT': 1020,
 'GT': 712,
 'TC': 432,
 'CA': 714,
 'AT': 819,
 'CG': 124,
 'AA': 866,
 'AG': 564,
 'GG': 362,
 'GA': 515,
 'AC': 703,
 'TA': 858,
 'CC': 285}
```


On peut aussi ne s'occuper que des répétitions d'une même base:

```python
compte_repets(gs, 2)

{'TT': 1020, 'AA': 866, 'GG': 362, 'CC': 285}
```

```python
compte_repets(gs, 4)

{'TTTT': 95, 'AAAA': 76, 'GGGG': 7}
```

## Jouons avec les noms et les prénoms


* Voici un fichier de 10 Mo contenant 
[800000 noms de familles](https://www.data.gouv.fr/fr/datasets/r/9ae80de2-a41e-4282-b9f8-61e6850ef449) recensés en France
en 2018.
* En voici un autre contenant 
[200000 prénoms](https://www.data.gouv.fr/fr/datasets/r/4b13bbf2-4185-4143-92d3-8ed5d990b0fa) 
recensés en France en 2018.
* Voici un fichier zippé contenant le [recensement les prénoms donnés en France depuis
  1900](https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2018_csv.zip)
  classés par départements.  Il contient 3,5 millions de lignes  !! **Il ne faut
  surtout pas l'ouvrir dans un tableur !!**
  
**On  voudrait utiliser  ces fichiers  pour faire  certaines recherches,  certains
classements.  Pour cela,  il faut  bien  avoir maîtrisé  tout  ce qui  a été  vu
auparavant dans le cours !**

**Vous présenterez votre  enquête dans un notebook interactif au  format `ipynb` que
vous pourrez  ensuite mettre  sur votre  compte gitlab et  vous me  fournirez le
lien.**


### Informations sur le dernier fichier 



Le fichier départemental comporte 3 624 994 enregistrements et cinq variables décrites ci-après.

Ce fichier est trié selon les variables `SEXE`, `PREUSUEL`, `ANNAIS`, `DPT`.

* Nom : `SEXE` - intitulé : `sexe` - Type : `caractère` - Longueur : 1 - Modalité : `1` pour masculin, `2` pour féminin
* Nom : `PREUSUEL` - intitulé : `premier prénom` - Type : `caractère` - Longueur : `25`
* Nom : `ANNAIS` - intitulé : `année de naissance` - Type : `caractère` - Longueur : `4` - Modalité : 1900 à 2018, `XXXX`
* Nom : `DPT` - intitulé : `département de naissance` - Type : `caractère` - Longueur : `3` - Modalité : liste des départements, `XX`
* Nom : `NOMBRE` - intitulé : `fréquence` - Type : `numérique` - Longueur : `8`




La refonte  du processus électoral a  entrainé un nombre de  corrections dans la
base des  prénoms plus important  que les  années précédentes. En  effet, chaque
électeur  est  maintenant  inscrit  au  répertoire  électoral  unique  avec  son
état-civil officiel (celui du Répertoire national d'identification des personnes
physiques / RNIPP), des anomalies ont donc été corigées.

Le fichier des prénoms est établi à  partir des seuls bulletins de naissance des
personnes   nées  en   France  (métropole   et  départements   d’outre-mer  hors
Mayotte).  En  conséquence,  l’exhaustivité  n’est pas  garantie  sur  toute  la
période, notamment pour les années antérieures à 1946. Les utilisateurs pourront
donc  constater des  écarts  avec le  nombre annuel  des  naissances évalué  par
l'Insee. Ces écarts, importants en début de période, vont en s’amenuisant. Après
1946, ils sont peu significatifs. 

Les  informations contenues  dans le  fichier des  prénoms sont  basées sur  les
bulletins d'état-civil  transmis à  l’Insee par les  officiers d’état  civil des
communes. Ces  bulletins sont  eux-mêmes établis à  partir des  déclarations des
parents.  L'Insee ne  peut  garantir  que le  fichier  des  prénoms soit  exempt
d'omissions ou d'erreurs.

### Pour comprendre

Pour chaque prénom, il est indiqué pour chaque année de naissance (de 1900 à 2018) et chaque sexe, le nombre de personnes inscrites à l'état civil sous ce prénom. Pour le fichier « DPT2018 », la précision est apportée pour chaque département.

#### Les personnes prises en compte

Le champ couvre l'ensemble des personnes nées en France hors Mayotte et enregistrées à l'état civil sur les bulletins de naissance. Les personnes nées à l'étranger sont exclues.

#### Le champ des prénoms retenus

Dans les fichiers de l’état civil, en l'occurrence les bulletins de naissance, les différents prénoms sont séparés par une espace (ou blanc). Ainsi deux prénoms séparés par un tiret constituent un seul prénom composé (exemple : Anne-Laure). Le premier prénom simple ou composé figure en début de liste, et c'est celui qui sera retenu après le traitement de la protection de l'anonymat.

#### Conditions portant sur les prénoms retenus

1. Sur la période allant de 1900 à 1945, le prénom a été attribué au moins 20 fois à des personnes de sexe féminin et/ou au moins 20 fois à des personnes de sexe masculin
2. Sur la période allant de 1946 à 2018, le prénom a été attribué au moins 20 fois à des personnes de sexe féminin et/ou au moins 20 fois à des personnes de sexe masculin
3. Pour une année de naissance donnée, le prénom a été attribué au moins 3 fois à des personnes de sexe féminin ou de sexe masculin

Les effectifs des prénoms ne remplissant pas les conditions 1 et 2 sont regroupés (pour chaque sexe et chaque année de naissance) dans un enregistrement dont le champ prénom (PREUSUEL) prend la valeur «_PRENOMS_RARES_». Les effectifs des prénoms remplissant la condition 2 mais pas la condition 3 sont regroupés (pour chaque sexe et chaque prénom) dans un enregistrement dont le champ année de naissance (ANNAIS) prend la valeur «XXXX».

#### Précision pour le département de naissance

Sur toute la période, le département de naissance (variable DPT) est celui existant au moment de la naissance. Ainsi une personne née à Issy-les-Moulineaux en 1949 sera codée en 75 (Seine), et une personne née en 1971 à Issy-les-Moulineaux sera codée en 92 (Hauts-de-Seine).

En effet, de par la loi n° 64-707 du 10/07/1964, il y a eu création des départements 75 (Paris), 78 (Yvelines), 91 (Essonne), 92 (Hauts-de-Seine), 93 (Seine-Saint-Denis), 94 (Val-de-Marne) et 95 (Val-d'Oise), à partir des anciens départements 75 (Seine) et 78 (Seine-et-Oise), avec une date d'effet dans le fichier au 1er janvier 1968.

*Deux regroupements de codes ont été faits pour la variable DPT* :

* le code DPT=20 est utilisé pour les naissances ayant eu lieu dans les deux départements 2A (Corse-du-Sud) et 2B (Haute-Corse) ;
* le code DPT=97 est utilisé pour les naissances ayant eu lieu en Guadeloupe, en MArtinique, en Guyane et à La Réunion.

#### Décrets et lois relatifs aux départements métropolitains

* Décret du 18/01/1955 : la Seine-Inférieure devient Seine-Maritime (76)

* Décret du 09/03/1957 : la Loire-Inférieure devient Loire-Atlantique (44)

* Loi n° 64-707 du 10/07/1964 : création des départements 75 (Paris), 78 (Yvelines), 91 (Essonne), 92 (Hauts-de-Seine), 93 (Seine-Saint-Denis), 94 (Val-de-Marne) et 95 (Val-d'Oise)

* Décret du 10/10/1969 : les Basses-Pyrénées deviennent Pyrénées-Atlantiques (64)

* Décret du 13/04/1970 : les Basses-Alpes deviennent Alpes-de-Haute-Provence (04)

* Loi n° 75-356 du 15/05/1975 : création des départements 2A (Corse-du-Sud) et 2B (Haute-Corse)

* Décret du 27/02/1990 : les Côtes-du-Nord deviennent Côtes-d'Armor (22)

#### Éléments de contexte sur le choix des prénoms

Il n'existe pas  de liste de prénoms autorisés. Les  prénoms connus étrangers ou
certains diminutifs peuvent ainsi être  choisis. En conséquence, on peut trouver
par exemple des prénoms à une seule  lettre dans le fichier des prénoms. Ce sont
effectivement  les  parents qui  choisissent  librement  le  ou les  prénoms  de
l’enfant  lors de  la  déclaration  de naissance.  En  particulier depuis  1993,
l’officier  d’état  civil  ne  peut  plus  refuser  le  prénom  choisi  par  les
parents. Il peut toutefois avertir le procureur de la République s’il estime que
le prénom nuit à l’intérêt de l’enfant (exemple : prénom ridicule ou grossier) ;
ou  que le  prénom méconnaît  le droit  d’un tiers  à voir  protéger son  nom de
famille (exemple  : un  parent ne peut  choisir comme prénom  le nom  de famille
d’une autre  personne dont l’usage  constituerait une usurpation).  Le procureur
peut  ensuite saisir  le  juge  aux affaires  familiales  qui  peut demander  la
suppression du prénom sur  les registres de l’état civil. Dans  ce cas le prénom
serait également supprimé du fichier des prénoms. 

L'alphabet  utilisé pour  l’écriture  des prénoms  doit être  celui  qui sert  à
l'écriture du  français. Les caractères  alphabétiques qui ne sont  pas utilisés
dans la langue française ne sont donc pas  autorisés (par exemple le « ñ » ou le
« ș »  ). Ainsi, par exemple,  le prénom d'origine roumaine  "Rareș" apparaît en
tant que "RARES" dans le fichier des prénoms. 


