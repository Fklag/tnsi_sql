---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---

1. [Tout ce que vous auriez dû savoir sur Python](1_Premiers_Pas)

1. [Algorithmes élémentaires à maîtriser](2_Premiers_exos)

1. [Récursion](3_Recursion)

1. [Calcul matriciel](4_Calcul_matriciel)

1. [Statistiques](5_Statistiques)

1. [Images](./TP_image/TP_image) et [corrigé](./TP_image_corr/TP_image_corr)

1. [Dichotomie](7_Dichotomie)

1. [Tris : épisode 1](8_tris_1)

1. [Cartes et big data](9_CarteElections2022)

1. [Tables et dictionnaires](10_MIB)


1. [Approximations de calcul d'intégrales](11_approxint)


1. [SQL](12_sql)
