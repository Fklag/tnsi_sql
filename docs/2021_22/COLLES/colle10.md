---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 10

**Semaine du 6 au 10 décembre 2021**

## Cours





### [Dérivées - Primitives](../../LATEX/PolyDerPrimInt21.pdf)

Approche  empiriquo-historique -  Nombre  dérivé  - Dérivabilité  en  un réel  -
Application au  calcul de  certaines limites  - Calcul  des dérivées  usuelles à
partir  de  la   définition  -  Approximation  affine  -   Dérivabilité  sur  un
intervalle  - Lien  avec  les  variations -  Fonctions  strictement monotones  -
Extremum local - Dérivée d'une composée - Primitives.


### [Initiation au calcul intégral](../../LATEX/PolyInt21.pdf)

Définition en  tant qu'aire -  Relation de Chasles  - Intégration par  parties -
Changement de variable (il sera donné s'il n'est pas simple).


### [Python](../../INFORMATIQUE/2_Premiers_exos/)

Exemples de  boucles : calculs de  sommes et de  produits - compteurs -  test de
présence de caractères  dans une chaîne - Recherches de  maximum - Histogramme -
Dessins à l'aide de  boucles imbriquées - Palindromes (ex 2-7  2-8 2-9 2-11 2-12
2-14). Récursion.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Fonction récursive déterminant si une chaîne de caractère est un palindrome.
- Dérivée de la fonction Arctan. Démonstration.
- Calcul  de la  dérivée de  la fonction inverse  à partir  de la  définition du
  nombre dérivé.
-  Calcul  de  primitives  de  $x\mapsto  \dfrac{2}{x\ln(x)}$  et  de  $x\mapsto
  \dfrac{16e^x}{(1+2e^x)^4}$
- Primitive de ln - Calcul de $\displaystyle\int_0^1\sqrt{1-t^2}dt$

## Exercices

On pourra poser une question impliquant l'écriture d'une fonction en Python.

Vous pouvez poser en colle tout exercice de la liste des chapitres 9 et 10 du Poly.

