---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 28

**Semaine du 7 au 10 juin 2022**

## Cours



### [Probabilités sur un ensemble fini](../../LATEX/Poly_IntroProbas21.pdf)

Expérience  aléatoire -  Vocabulaire  - Système  complet  d'évènements -  Espace
probabilisé  : 3  axiomes de  Kolmogorov -  Équiprobabilité -  Probabilité d'une
union  - Croissance  d'une  mesure  de probabilité  -  Formule des  probabilités
totales -  Conditionnement -  Formule des probabilités  composées -  Formules de
Bayes - Indépendance : mutuelle et 2 à 2.



### [Variables aléatoires réelles finies](../../LATEX/PolyVar21.pdf)

Loi de probabilité -  SCE lié à une VAR - Existence d'une  loi de valeurs prises
connues - Fonction de répartition - Fonction d'une VAR - Espérance - Théorème de
transfert - Linéarité de l'espérance - Moments d'ordre $r$ - Moments centrés - Fonction
indicatrice et  probabilité - Variance -  Théorème de K-H -  $\mathbb V(aX=b)$ -
VAR centrée réduite - VA certaine - Loi uniforme sur $[\![ 1, n]\!]$,
sur $[\![ a, b ]\!]$ - Loi binomiale.

### Python
 

#### [Tris et dictionnaires](../../INFORMATIQUE/10_MIB/)

Dictionnaires - Parcours de dictionnaires - Construction par compréhension.


## Questions de cours


- On posera toute question des exercices I et IΙ du dernier [DS](../../DS/DS9_21.pdf) 

- Formule de KH : énoncé et démonstration

- $\mathbb E(aX+b)$ et $\mathbb V(aX+b)$ : énoncé et démonstration

- Loi uniforme sur $[\![ 1, n\rrbracket]\!], espérance et variance

- Une  urne contient $p$  boules numérotées  de 1 $p$.  On tire $n$  boules avec
  remise et on note X le plus grand numéro obtenu. Déterminer le loi de X.

## Exercices


Vous pouvez poser en colle tout exercice de la liste des chapitres 24 et 25
 du  Poly  et  du   TP  d'info  (en  particulier  exercices  de
 manipulation de dictionnaires).  Vous pouvez également poser un exercice sur le
 chapitre `numpy.randint(1, 24)`.
 
 
 
