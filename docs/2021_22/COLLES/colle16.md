---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 16

**Semaine du 21 janvier au 25 février 2022**

## Cours






### [Polynômes](../../LATEX/PolyPoly21.pdf)

Rappels  sur le  2nd  degré. Définition,  addition  et multiplication.  Racines,
divisibilté, ordre de multiplicité.

### [Espaces vectoriels (début)](../../LATEX/PolyEV21.pdf)

Définition d'un $\mathbb K$-espace vectoriel (pour information - pas d'exercices
dessus). Cinq exemples de réfŕence. Sous-espaces vectoriels - Caractérisations. 
SEV engendré par une famille - Famille génératrice.

### Python

#### [Calcul matriciel](../../INFORMATIQUE/cal_mat/) et [manipulation d'images](../../INFORMATIQUE/image_mat/)

Récursion plus début du calcul matriciel : calcul de la trace, de la
transposée. Test  pour déterminer  si une  matrice est  triangulaire supérieure,
symétrique.    Produit    matriciel,    puissance    (version    récursive    et
impérative). Manipulation d'images  en tant que matrice :  fonction agissant sur
les pixels, retournements.



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Fonction Python déterminant le degré d'un polynôme (liste des coefficients dans l'ordre croissant des degrés) en créant d'abord une fonction qui normalise la liste (en enlevant les zéros non significatifs).
- Fonction Python qui additionne deux polynômes en disposant d'une fonction `degré` et `normalise`.
- Fonction Python qui multiplie deux polynômes en disposant d'une fonction `degré` et `normalise`.
- Déterminer les polynômes de $ℝ[X]$ tels que $P(x^2)=(x^2+1)×P(x)$.
-   Soit  $\mathscr   F$   une   famille  de   vecteurs   d'un  $\mathbb   K$-EV
  E. Vect($\mathscr F$) est un SEV de E.
- Toute sur-famille d'une famille génératrice d'un $\mathbb K$-EV E est encore génératrice de E.
- Vect$(((1,2), (1,0))) = \mathbb R^2$
## Exercices


Vous pouvez poser en colle tout exercice de la liste des chapitres 16 et 17  du Poly.

