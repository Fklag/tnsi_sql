---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 14

**Semaine du 24 au 28 janvier 2022 - Bienvenue au second semestre**

## Cours



### [Géométrie](../../LATEX/PolyGeo21.pdf)

Rappels sur le calcul  vectoriel de 2nde - Rappels sur  le produit scalaire dans
le  plan  - Définition  et  autres  formulations  -  Bilinéarité et  symétrie  -
Orthogonalité - Produit scalaire dans  l'espace et applications - Vecteur normal
à un  plan - Distance  d'un point à un  plan - Représentations  paramétriques de
droites et de plans - Équation cartésienne d'un plan - Cercles et sphères.



### [Statistique descriptive](../../LATEX/PolyStats21.pdf)

Statistque univariée et bivariée.


### [Polynômes](../../LATEX/PolyPoly21.pdf)

Juste définition, addition et multiplication et rapide survol du programme de 1ère sur le second degré.

### Python

#### [Calcul matriciel](../../INFORMATIQUE/cal_mat/)

Récursion plus début du calcul matriciel : calcul de la trace, de la
transposée. Test  pour déterminer  si une  matrice est  triangulaire supérieure,
symétrique. Produit matriciel, puissance (version récursive et impérative)

#### [statistiques](../../INFORMATIQUE/5_Statistiques/)

Moyenne, moyenne du produit puis calcul  de la covariance avec König-Huyghens puis
de la variance.  Détermination du coefficient de corrélation et  de la droite de
régression.
Toute fonction de ce TP est à chercher et peut être demandée en colle.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Formule et démonstration de $\mathbb V(x+y)$.
- Formule de König-Huyghens pour le calcul de $\mathbb V(x)$.
- Formule de König-Huyghens pour le calcul de $\mathbb S(x,y)$.
- Fonction déterminant la covariance avec toute fonction intermédiaire nécessaire.
- Fonction Python déterminant le degré d'un polynôme (liste des coefficients dans l'ordre croissant des degrés) en créant d'abord une fonction qui normalise la liste (en enlevant les zéros non significatifs).
- Fonction Python qui additionne deux polynômes en disposant d'une fonction `degré` et `normalise`.
- Fonction Python qui multiplie deux polynômes en disposant d'une fonction `degré` et `normalise`.


## Exercices

En statistique, on posera une question impliquant l'écriture d'une ou plusieurs fonctions en Python.

Vous pouvez poser en colle tout exercice de la liste des chapitres 14, 15 et 16 (partie 1ère jusqu'à l'ex. 36) du Poly.

