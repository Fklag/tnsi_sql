---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 12

**Semaine du 10 au 15 janvier 2022 (Semaine de DS)**

## Cours

### [Équations différentielles](../../LATEX/PolyED21.pdf)

Équations différentielles linéaires  du 1er ordre - Équations  homogènes et avec
second  membre  - Principe  de  superposition  des  solutions  - Méthode  de  la
variation de  la constante. Équations  linéaires du second ordre  à coefficients
constants - Équation  caractéristique associée - Pour les  équations avec second
membre,  on  donnera  la  forme d'une  solution  particulière.  Résolution  avec
SymPy. La méthode d'Euler sera étudiée dans un prochain TP.

### [Calcul matriciel](../../LATEX/PolyMat21.pdf)

Introduction historique  - Définition - Utilisation  de listes de listes  et des
`array`       de       NumPy        -       Matrices       particulières       :
ligne  /  colonne  /  carrée  /  nulle /  diagonale  /  scalaire  /  identité  /
triangulaire / symétrique / antisymétrique / élémentaire / échelonnée.

Opérations sur les matrices - Trace  et transposée d'un produit - Distributivité
/ associativité  du produit matriciel  - Puissance (idempotence /  nilpotence) -
Matrices inversibles - Déterminant et inverse éventuel d'une matrice $2×2$. 


### [Systèmes linéaires](../../LATEX/PolySystemes21.pdf)

Exemple historique - Écriture matricielle  - Systèmes équivalents - Recherche de
l'inverse  - Rang  d'une  matrice -  Méthode de  Gauss-Jordan  - Utilisation  de
`NumPy` et `SymPy`.


### [Python](../../INFORMATIQUE/4_Calcul_matriciel/#poly)

Récursion plus début du calcul matriciel : calcul de la trace, de la
transposée. Test  pour déterminer  si une  matrice est  triangulaire supérieure,
symétrique. Produit matriciel.

## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- La matrice $\mathbb I_n$ est neutre pour le produit matriciel.
- Transposée d'un produit.
- Trace d'un produit.
- Fonction Python  renvoyant le produit de deux matrices  données sous forme de
  listes de listes.
- Fonction Python renvoyant la transposée d'une matrice.
- Résoudre dans $ℝ^3$ le système $\left\{\begin{array}{c}mx+y+z=m\\
x+my+z=m\\
x+y+mz=m
\end{array}\right.$

## Exercices

On pourra poser une question impliquant l'écriture d'une fonction en Python.

Vous pouvez poser en colle tout exercice de la liste des chapitres 11, 12 et 13 du Poly.

