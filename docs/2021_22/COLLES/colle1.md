---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 1

**Semaine du 20 au 24 septembre 2021**

## Cours

### [Bases pour raisonner](../../COURS/1_Bases_logique_sommes/#chapitre-1-bases-pour-raisonner)

Logique - Implication - Contraposée -  Négation - Quantificateurs - Réciproque -
Équivalence - Récurrence simple, double, forte - Analyse Synthèse.

### [Ensembles](../../COURS/2_sets/#lets-talk-about-sets)

Appartenance - Inclusion - Égalité - Type `Set` en python - Définition par extension/compréhension -
Ensembles de  nombres -  Cardinal d'un  ensemble fini -  Ensemble des  parties -
Opérations  sur  les  ensembles  -  Duale  d'une  équation  sur  $\scr  P(E)$  -
Partition - Produit cartésien.


### [Premiers pas en Python](../../INFORMATIQUE/1_Premiers_Pas/#chapitre-1-tout-ce-que-vous-auriez-du-savoir-sur-python)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles - Type `List` - Chaînes de caractères.

## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Tout exercice du [test de rentrée](../../DS/TestRentree.pdf)
- $\sqrt{2}$ est-il rationnel ? (exercice 1-14)
- 4 propositions de l'[exercice 1-15](../../EXERCICES/1_Bases_logique/#logique)
- Soit $(u_n)_{n\inℕ}$ la suite réelle  définie par $u_0=2,\ u_1=7$ et
         $u_{n+2}=7u_{n+1}-10u_n$.  Démontrer que $(∀n ∈ ℕ)(u_n=2^n+5^n)$
- Énoncer et démontrer par récurrence la formule donnant la somme des carrés des
  entiers de $1$ à $n$.


## Exercices

On posera au moins un exercice sur le chapitre *Ensembles*.
