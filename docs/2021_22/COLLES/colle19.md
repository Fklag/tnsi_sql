---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 19

**Semaine du $π$ au 18 mars 2022**

## Cours






### [Suites (suite)](../../LATEX/PolySuitesConv21.pdf)

Suites bornées  - Sens de  variation - Convergence,  divergence - Unicité  de la
limite  - Toute  suite convergente  est bornée  - Opérations  sur les  limites -
Passage  à la  limite  des inégalités  - Théorème  d'encadrement  des limites  -
Théorème de  comparaison des limites  infinies - Suite croissante  convergente -
Théorème des  suites monotones -  Croissances comparées - Suites  équivalentes -
Définition - Caractérisation par la limite  du rapport - Rapports avec le signe,
les limites  - Opérations sur les  équivalents - Équivalents usuels  à partir de
suites convergeant  vers 0  (limites de taux  de variations) -  Limite de  $(1 +
1/n)^n$ - 
Suites adjacentes.

### [Limites et continuité](../../LATEX/PolyLimCont21.pdf)

Notion de  voisinage - Définitions  des différentes  limites - Continuité  en un
réel - Opérations sur les limites -
Composition -  Relation d'ordre  - Encadrement -  Prolongement par  continuité -
Limites et  suites - Théorème de  la limite monotone -  Fonctions équivalentes -
Opérations sur les équivalents - Équivalents usuels.


### Python
 
#### [Dichotomie](../../INFORMATIQUE/7_Dichotomie)

 Recherche  séquentielle  - Recherche  dichotomique  -  Résolution d'équation  -
 Exemple de recherche d'une valeur approchée de $\sqrt{2}$.



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Fonction Python `dicho(f, a, b,  eps)` calculant une approximation de $f(x)=0$
  sur un intervalle 
  $[a,b]$ où $f$ est continue et strictement croissante avec une précision
  arbitraire   `eps`   (supérieure   à   $2^{52}$)  à   l'aide   d'une   méthode
  dichotomique. En donner une version récursive et une autre impérative.
- $\displaystyle \lim_{n\to+\infty}\left(1+\dfrac{1}{n} \right)^n$.
- $\displaystyle\lim_{n\to+\infty}s_n$ avec $s_n=\displaystyle\sum_{k=1}^n\dfrac{n}{n^2+k}$ et fonction
Python donnant une approximation de $s_n$ pour un entier $n$ donné en paramètre.
- Équivalent de $\cos(x) - 1$ au voisinage de 0 : énoncé et preuve.
- $\displaystyle\lim_{x\to2}\dfrac{\sqrt{x^2+1}-\sqrt{x+3}}{x^2-2x}$.
- $0^0$ et $\infty^0$ sont des formes indéterminées : exemples.


## Exercices


Vous pouvez  poser en colle  tout exercice  de la liste  des chapitres 18  et 19
(continuité sur un intervalle non comprise) du Poly.  On évitera les exercices exigeant des
démonstrations « avec  des $ε$ ». La dichotomie a  été étudiée expérimentalement
en info : pas encore de théorème de la bijection.

