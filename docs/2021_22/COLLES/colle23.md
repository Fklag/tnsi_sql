---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 23

**Semaine du 25 au 29 avril 2022**

## Cours



### [Développements limités](../../LATEX/PolyDL21.pdf)


Fonction négligeable devant une autre au voisinage  d'un point - DL - Formule de
Taylor-Young  -  DL de  référence  -  Opérations (somme,  produit,  intégration,
substitution) - Applications à l'étude locale
d'une fonction (asymptotes, tangentes, positions relatives,...).

### [Intégration 2](../../LATEX/PolyInt2_21.pdf)

Rappels sur  le chapitre 10 (définition  en termes d'aire, relation  de Chasles,
linéarité,  IPP, changement  de variable)  -  Intégrales et  relation d'ordre  -
Valeur moyenne  - Théorème fondamental de  l'analyse - Fonction définie  par une
intégrale -  Somme de Riemann :  théorème admis pour les  fonctions continues et
démonstration  pour des  fonctions  de classe  $\mathscr  C^1$ -  Interprétation
géométrique.

### Python
 

#### [Tris](../../INFORMATIQUE/8_tris_1/)

Tri à bulle - Tri par insertion - Tri par sélection - Tri par comptage.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Tri à bulle. Savoir le commenter.
- Tri par sélection. Savoir le commenter.
- Tri par comptage. Savoir le commenter.
- $DL_5(0)$ de $\tan(x)$.
- $f(x)=\dfrac{1}{\sin x}- \dfrac{1}{x}$. Démontrer  que $f$ peut être prolongée
  par continuité en 0, que ce prolongement  par continuité est dérivable en 0,
  former  l'équation de  la tangente  $(T)$ à  $\mathscr{C}_f$ et  déterminer la
  position relative de $(T)$ et de $\mathscr{C}_f$ au voisinage de 0. 
- Convergence  des sommes  de Riemann  : démonstration de  la convergence  de la
  suite $(S_n)$ dans le cas d'une fonction $\mathscr C^1$ sur un intervalle $[a,b]$.
-     Sens     de     variation      de     la     fonction     $G:     x\mapsto
  \displaystyle\int_x^{2x}\dfrac{dt}{t^2+t+1}$.
  
## Exercices


Vous pouvez  poser en colle  tout exercice  de la liste  des chapitres 21  et 22
 du Poly et du TP d'info jusqu'au tri par comptage.
