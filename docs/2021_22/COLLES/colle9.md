---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 9

**Semaine du 29 novembre au 3 décembre 2021**

## Cours





### [Généralités sur les fonctions](../../LATEX/PolyGenFonc21.pdf)

-  Cours  de 2nde  :  définition  d'une  fonction,  lecture graphique,  sens  de
  variation, images/antécédents, etc.
- Survol des fonctions usuelles  : trigonométriques, exp, ln, puissances, valeur
  absolue, partie entière.
- Les  calculs de  dérivée et  de limites ne  sont pas  encore un  objectif. Ils
  doivent  être  simples.  L'idée  est de  résoudre  des  problèmes  algébriques
  impliquant les fonctions usuelles avec  des méthodes algébriques ou des outils
  analytiques  élémentaires  et  d'étudier  des fonctions  sans  grand  théorème
  d'analyse. On pourra demander les commandes Python permettant
  de tracer des représentations graphiques.


### [Dérivées - Primitives](../../LATEX/PolyDerPrimInt21.pdf)

Approche  empiriquo-historique -  Nombre  dérivé  - Dérivabilité  en  un réel  -
Application au  calcul de  certaines limites  - Calcul  des dérivées  usuelles à
partir  de  la   définition  -  Approximation  affine  -   Dérivabilité  sur  un
intervalle  - Lien  avec  les  variations -  Fonctions  strictement monotones  -
Extremum local - Dérivée d'une composée - Primitives.


### [Python](../../INFORMATIQUE/2_Premiers_exos/)

Exemples de  boucles : calculs de  sommes et de  produits - compteurs -  test de
présence de caractères  dans une chaîne - Recherches de  maximum - Histogramme -
Dessins à l'aide de  boucles imbriquées - Palindromes (ex 2-7  2-8 2-9 2-11 2-12
2-14). 


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

- Déterminez une fonction  Python qui prend une liste de  nombres en argument et
  renvoie un couple contenant l'indice de la première occurence du maximum et sa
  valeur.  
- Étude de  la fonction tan :  réduction du domaine d'étude,  sens de variation,
  démonstration de la formule de la dérivée, tracé sur trois périodes.
- Dérivée de la fonction Arctan. Démonstration.
- Démontrer que la fonction $x\mapsto (x-1)^3+42$ admet le point $A(1,42)$ comme
  centre de symétrie. 
- $ \displaystyle\lim_{x\to 0}\dfrac{\sqrt{1+x}-1}{x}$
- Calcul  de la  dérivée de  la fonction inverse  à partir  de la  définition du
  nombre dérivé.
- Calcul de primitives de $x\mapsto \dfrac{2}{x\ln(x)}$ et de $x\mapsto \dfrac{16e^x}{(1+2e^x)^4}$

## Exercices

On posera au moins un exercice impliquant l'écriture d'une fonction en Python.

Vous pouvez poser en colle tout exercice de la liste des chapitres 8 et 9 du Poly.

