---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 27

**Semaine du 30 mai au 3 juin 2022**

## Cours



### [Probabilités sur un ensemble fini](../../LATEX/Poly_IntroProbas21.pdf)

Expérience  aléatoire -  Vocabulaire  - Système  complet  d'évènements -  Espace
probabilisé  : 3  axiomes de  Kolmogorov -  Équiprobabilité -  Probabilité d'une
union  - Croissance  d'une  mesure  de probabilité  -  Formule des  probabilités
totales -  Conditionnement -  Formule des probabilités  composées -  Formules de
Bayes - Indépendance : mutuelle et 2 à 2.

### Python
 

#### [Tris et dictionnaires](../../INFORMATIQUE/10_MIB/)

Dictionnaires - Parcours de dictionnaires - Construction par compréhension.


## Questions de cours

On  commencera  la  colle  par  toute  question sur  un  point  du  chapitre  24
(définition,  théorème, exemple)  ou un  tri.   On pourra  par exemple  demander
plusieurs énoncés de théorèmes ou de définitions en veillant à bien vérifier les
conditions d'utilisation.

On posera toute question des exercices I et IΙ du dernier [DS](../../DS/DS9_21.pdf) 

## Exercices


Vous pouvez  poser en colle  tout exercice  de la liste  du chapitre  24
 du  Poly qui  correspond au  cours vu  et  du   TP  d'info  (en  particulier  exercices  de
 manipulation  de dictionnaires).  Vous  poserez également  un  exercice sur  le
 chapitre `numpy.randint(1, 24)`.
 
 
 
