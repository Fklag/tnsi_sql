---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 22

**Semaine de DS du 4 au 8 avril 2022**

## Cours


### [Dérivation : approfondissement](../../LATEX/PolyDerApp21.pdf)

Révisions  sur  les  chapitres  8  et 9  (nombre  dérivé,  calcul  de  dérivées,
dérivation et  variations) - Dérivées  successives :  la formule de  Leibniz est
hors  programme  mais a  été  vue  (on peut  la  rappeler  pour un  exercice)  -
Dérivation et extrema  - Théorème de Rolle - Égalité  des accroissements finis -
L'inégalité ne doit pas être sue mais  doit pouvoir être redémontrée à partir de
l'EAF - Application aux suites définies par une relation de récurrence.

### [Développements limités](../../LATEX/PolyDL21.pdf)


Fonction négligeable devant une autre au voisinage  d'un point - DL - Formule de
Taylor-Young  -  DL de  référence  -  Opérations (somme,  produit,  intégration,
substitution) - Applications à l'étude locale
d'une fonction (asymptotes, tangentes, positions relatives,...).

### Python
 

#### [Tris](../../INFORMATIQUE/8_tris_1/)

Tri à bulle. Tri par insertion.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Tri à bulle. Savoir le commenter.
-  Égalité des  accroissements finis  : énoncé  et démonstration.  Application à
  l'inégalité des accroissements finis.
- Théorème de Rolle : énoncé et démonstration.
- Dérivée de $\ln\circ\ln\circ\cdots\circ\ln$ ($n$ compositions avec $n∈ℕ$).
- $DL_5(0)$ de $\tan(x)$.
- $f(x)=\dfrac{1}{\sin x}- \dfrac{1}{x}$. Démontrer  que $f$ peut être prolongée
  par continuité en 0, que ce prolongement  par continuité est dérivable en 0,
  former  l'équation de  la tangente  $(T)$ à  $\mathscr{C}_f$ et  déterminer la
  position relative de $(T)$ et de $\mathscr{C}_f$ au voisinage de 0. 
- Règles de calcul sur les $\mathscr o$. Preuve d'au moins 3 d'entre elles.
  
## Exercices


Vous pouvez  poser en colle  tout exercice  de la liste  des chapitres 20  et 21
 du Poly.
