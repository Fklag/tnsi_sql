---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 5

**Semaine du 18 au 23 octobre 2021**

## Cours



### [Complexes](../../LATEX/PolyComplexe21_p.pdf)

Approche historique via  Caspar WESSEL - Définition de $ℂ$  - Forme algébrique -
Conjugué  -  Module  -  Résolution  de  $X^2=α$  dans  le  cas  $α∈ℝ$  et  aussi
$α∈ℂ\setminus ℝ$ à  titre d'exercice - Résolution d'équations du  second degré à
coefficients  réels  et  aussi  à coefficients  complexes  quelconques  à  titre
d'exercice  en étant  guidé  - Formes  trigonométriques  et exponentielles  d'un
nombre complexe.

### [Dénombrements](../../LATEX/PolyDenomb21_p.pdf)

Calculs de sommes et de produits. Notations Σ et Π.


### [Python](../../INFORMATIQUE/2_Premiers_exos/)

Exemples de boucles : calculs de sommes et de produits - compteurs.



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:


- Toute question de cours du [TD 2](../../LATEX/TD2_complexes.pdf)
- Toute question du [DS2](../../DS/DS2_21.pdf) sauf **III** 6) et 7)
- Forme exponentielle de $1-e^{iα}$
- $\displaystyle\sum_{k=0}^n\cos(kx)$
- Calcul de $\displaystyle \sum_{i=1}^p\left(\sum_{k=1}^nk×i\right)$.
- Énoncé et démonstration de la linéarité de la somme.

## Exercices

On  insistera sur  les calculs  et  la présentation  rigoureuse des  résolutions
d'(in)équations.



