---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 8

**Semaine du 22 au 26 novembre 2021**

## Cours



### [Généralités sur les suites réelles](../../LATEX/PolySuites_1_21.pdf)

Définitions    -    Opérations    -    Suites    arithmétiques,    géométriques,
arithmético-géométriques,   récurrentes  linéaires   d'ordre  2   (formule  sans
démonstration).


### [Généralités sur les fonctions](../../LATEX/PolyGenFonc21.pdf)

-  Cours  de 2nde  :  définition  d'une  fonction,  lecture graphique,  sens  de
  variation, images/antécédents, etc.
- Survol des fonctions usuelles  : trigonométriques, exp, ln, puissances, valeur
  absolue, partie entière.
- Les  calculs de  dérivée et  de limites ne  sont pas  encore un  objectif. Ils
  doivent  être  simples.  L'idée  est de  résoudre  des  problèmes  algébriques
  impliquant les fonctions usuelles avec  des méthodes algébriques ou des outils
  analytiques  élémentaires  et  d'étudier  des fonctions  sans  grand  théorème
  d'analyse. On pourra demander les commandes Python permettant
  de tracer des représentations graphiques.




### [Python](../../INFORMATIQUE/2_Premiers_exos/)

Exemples de  boucles : calculs de  sommes et de  produits - compteurs -  test de
présence de caractères  dans une chaîne - Recherches de  maximum - Histogramme -
Dessins à l'aide de  boucles imbriquées - Palindromes (ex 2-7  2-8 2-9 2-11 2-12
2-14). 


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

- Déterminez une fonction Python qui prend une liste de nombres en argument et renvoie un couple contenant l'indice de la première occurence du maximum et sa valeur. 
- On considère la suite $(u_n)$ définie par : $u_0=1$,
  $u_1=\sqrt{3}-\dfrac{1}{2}$     et      $\forall     n     \in      ℕ,     \:
  u_{n+2}=-u_{n+1}-u_{n}$. Exprimer son terme général. 
- Formule et  démonstration du calcul  de la somme des termes successifs d'une
  suite arithmétique.
-  Étude  général d'une  suite  arithmético-géométrique  : démonstration  de  la
  formule (car il faut connaître le principe et pas la formule).
- Étude de  la fonction tan :  réduction du domaine d'étude,  sens de variation,
  démonstration de la formule de la dérivée, tracé sur trois périodes.
- Démontrer que la fonction $x\mapsto (x-1)^3+42$ admet le point $A(1,42)$ comme
  centre de symétrie. 
- Sens  de variation d'une  composée de deux fonctions  monotones. Démonstration
  dans le cas où les deux fonctions ont des ens de variations distincts.


## Exercices

On posera au moins un exercice impliquant l'écriture d'une fonction en Python.

Vous pouvez poser en colle tout exercice de la liste des chapitres 7 et 8 du Poly.

