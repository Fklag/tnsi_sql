---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 17

**Semaine du 28 février au 4 mars 2022**

## Cours





### [Espaces vectoriels (début)](../../LATEX/PolyEV21.pdf)

Définition d'un $\mathbb K$-espace vectoriel (pour information - pas d'exercices
dessus). Cinq exemples de référence. Sous-espaces vectoriels - Caractérisations. 
SEV engendré  par une  famille - Famille  génératrice - Famille  libre -  Base -
Dimension  - Une  famille libre  (ou génératrice)  de cardinal  la dimension  de
l'espace est une base - Bases  canoniques de $\mathbb K^n$, de $\mathbb K_n[X]$,
de $\mathfrak M_n(\mathbb K)$.

### Python

#### [Manipulation d'images](../../INFORMATIQUE/TP_image/TP_image/)

 Manipulation d'images  en tant que  matrice : jusqu'à la  luminance (changement
 d'énergie non compris).



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

-   Soit  $\mathscr   F$   une   famille  de   vecteurs   d'un  $\mathbb   K$-EV
  E. Vect($\mathscr F$) est un SEV de E.
- Toute sur-famille  d'une famille génératrice d'un $\mathbb K$-EV  E est encore
  génératrice de E.
- Toute sous-famille d'une famille libre d'un $\mathbb K$-EV E est encore libre dans E.
-  Soit  $E$  l'ensemble  des  suites  réelles  qui  vérifient  la  relation  de
    récurrence: $\forall n \in \mathbb N$, $ u _{n+2} = u_{n+1} + u_n $.
	1. Montrer que $E$ est un espace vectoriel.
    2.  En déterminer une base.
- Soit  $f$ et $g$ les  fonctions définies sur $\mathbb  R$ par $ \forall  x \in
  \mathbb R $, $
  f(x) =  e^x $ et  $ g(x) =  e^{2x} $. La  famille $(f,g)$ est-elle  libre dans
  l'espace vectoriel des fonctions réelles? 
- Donner une base du SEV de $\mathfrak{M}_4(\mathbb R)$ des matrices qui sont
  antisymétriques.
  
## Exercices


Vous pouvez poser en colle tout exercice de la liste des chapitres 17 du Poly.

