---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 11

**Semaine du 3 au 7 janvier 2022 : bonne année !**

## Cours

### [Équations différentielles](../../LATEX/PolyED21.pdf)

Équations différentielles linéaires  du 1er ordre - Équations  homogènes et avec
second  membre  - Principe  de  superposition  des  solutions  - Méthode  de  la
variation de  la constante. Équations  linéaires du second ordre  à coefficients
constants - Équation  caractéristique associée - Pour les  équations avec second
membre,  on  donnera  la  forme d'une  solution  particulière.  Résolution  avec
SymPy. La méthode d'Euler sera étudiée dans un prochain TP.

### [Calcul matriciel](../../LATEX/PolyMat21.pdf)

Introduction historique  - Définition - Utilisation  de listes de listes  et des
`array`       de       NumPy        -       Matrices       particulières       :
ligne  /  colonne  /  carrée  /  nulle /  diagonale  /  scalaire  /  identité  /
triangulaire / symétrique / antisymétrique / élémentaire / échelonnée.

Opérations sur les matrices - Trace  et transposée d'un produit - Distributivité
/ associativité  du produit matriciel  - Puissance (idempotence /  nilpotence) -
Matrices inversibles - Déterminant et inverse éventuel d'une matrice $2×2$. 



### [Python](../../INFORMATIQUE/3_Recursion/)

- Les exercices ont été corrigés jusqu'à l'avant-dernier. Des pistes ont été
données pour le calcul  de $A_n^p$ qui peut être posé en  exercice tout comme le
calcul du nombre de combinaisons.

## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- La matrice $\mathbb I_n$ est neutre pour le produit matriciel.
- Transposée d'un produit.
- Trace d'un produit.
- Fonction Python  renvoyant le produit de deux matrices  données sous forme de
  listes de listes.
- Principe de superposition des solutions des équations différentielles linéaires du premier
  ordre : énoncé et démonstration.


## Exercices

On pourra poser une question impliquant l'écriture d'une fonction en Python.

Vous pouvez poser en colle tout exercice de la liste des chapitres 11 et 12 du Poly.

Concernant le  calcul matriciel, l'étude  des systèmes linéaires n'a  pas encore
été  vue  :  le  calcul  éventuel   d'inverses  ne  doit  pas  faire  intervenir
l'algorithme de Gauss-Jordan.
