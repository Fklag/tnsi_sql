---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 13

**Semaine du 17 au 21 janvier 2022**

## Cours



### [Systèmes linéaires](../../LATEX/PolySystemes21.pdf)

Exemple historique - Écriture matricielle  - Systèmes équivalents - Recherche de
l'inverse  - Rang  d'une  matrice -  Méthode de  Gauss-Jordan  - Utilisation  de
`NumPy` et `SymPy`.

### [Géométrie](../../LATEX/PolyGeo21.pdf)

Rappels sur le calcul  vectoriel de 2nde - Rappels sur  le produit scalaire dans
le  plan  - Définition  et  autres  formulations  -  Bilinéarité et  symétrie  -
Orthogonalité - Produit scalaire dans  l'espace et applications - Vecteur normal
à un  plan - Distance  d'un point à un  plan - Représentations  paramétriques de
droites et de plans - Équation cartésienne d'un plan - Cercles et sphères.


### [Python](../../INFORMATIQUE/4_Calcul_matriciel/#poly)

Récursion plus début du calcul matriciel : calcul de la trace, de la
transposée. Test  pour déterminer  si une  matrice est  triangulaire supérieure,
symétrique. Produit matriciel.

## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Fonction Python testant si une matrice est symétrique.
- Fonction Python renvoyant la transposée d'une matrice.
- Résoudre dans $ℝ^3$ le système $\left\{\begin{array}{c}mx+y+z=m\\
x+my+z=m\\
x+y+mz=m
\end{array}\right.$
- Une ou plusieurs démonstration du Vrai ou Faux suivant:
L'espace est muni d'un repère orthonormal.

On considère la droite ($d$) dont un système d'équations paramétriques est :
 $\left\{\begin{array}{l c l}
 x&=&2 - \dfrac{t}{2}\\
 y &=& 1\\ 
 z &=&5 - \dfrac{3t}{2}\\
\end{array}\right. ~~(t \in \mathbb R)$

On note  A le  point de  coordonnées $(2~;~-1~;~1)$, B  le point  de coordonnées
$(4~;~-2~;~2)$ et C le point de ($d$) d'abscisse $1$.

1. La droite ($d$) est parallèle à l'axe $\left(\text{O}~;~\overrightarrow{\jmath}\right)$.

2.  Le plan $P$ d'équation $x+ 3z - 5=0$ est le plan passant par A et orthogonal à ($d$).

3. La mesure de l'angle géométrique $\widehat{\text{BAC}}$ est $\dfrac{\pi}{3}$ radians.

4. La sphère de centre C et passant par B coupe le plan $P$ d'équation $x+3z -5 = 0$.



## Exercices

On pourra poser une question impliquant l'écriture d'une fonction en Python.

Vous pouvez poser en colle tout exercice de la liste des chapitres 13 et 14 du Poly.

