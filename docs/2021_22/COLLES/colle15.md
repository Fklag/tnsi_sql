---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 15

**Semaine du 31 janvier au 4 février 2022**

## Cours




### [Statistique descriptive](../../LATEX/PolyStats21.pdf)

Statistque univariée et bivariée.


### [Polynômes](../../LATEX/PolyPoly21.pdf)

Rappels  sur le  2nd  degré. Définition,  addition  et multiplication.  Racines,
divisibilté, ordre de multiplicité.

### Python

#### [Calcul matriciel](../../INFORMATIQUE/cal_mat/) et [manipulation d'images](../../INFORMATIQUE/image_mat/)

Récursion plus début du calcul matriciel : calcul de la trace, de la
transposée. Test  pour déterminer  si une  matrice est  triangulaire supérieure,
symétrique.    Produit    matriciel,    puissance    (version    récursive    et
impérative). Manipulation d'images  en tant que matrice :  fonction agissant sur
les pixels, retournements.:

#### [statistiques](../../INFORMATIQUE/5_Statistiques/)

Moyenne, moyenne du produit puis calcul  de la covariance avec König-Huyghens puis
de la variance.  Détermination du coefficient de corrélation et  de la droite de
régression.
Toute fonction de ce TP est à chercher et peut être demandée en colle.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Formule et démonstration de $\mathbb V(x+y)$.
- Formule de König-Huyghens pour le calcul de $\mathbb V(x)$.
- Formule de König-Huyghens pour le calcul de $\mathbb S(x,y)$.
- Fonction déterminant la covariance avec toute fonction intermédiaire nécessaire.
- Fonction Python déterminant le degré d'un polynôme (liste des coefficients dans l'ordre croissant des degrés) en créant d'abord une fonction qui normalise la liste (en enlevant les zéros non significatifs).
- Fonction Python qui additionne deux polynômes en disposant d'une fonction `degré` et `normalise`.
- Fonction Python qui multiplie deux polynômes en disposant d'une fonction `degré` et `normalise`.
- Déterminer les polynômes de $ℝ[X]$ tels que $P(x^2)=(x^2+1)×P(x)$.

## Exercices

En statistique, on posera une question impliquant l'écriture d'une ou plusieurs fonctions en Python.

Vous pouvez poser en colle tout exercice de la liste des chapitres 15 et 16  du Poly.

