---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 2

**Semaine du 27 septembre au 1er octobre 2021**

## Cours

### [Bases pour raisonner](../../COURS/1_Bases_logique_sommes/#chapitre-1-bases-pour-raisonner)

Logique - Implication - Contraposée -  Négation - Quantificateurs - Réciproque -
Équivalence - Récurrence simple, double, forte - Analyse Synthèse.

### [Ensembles - Applications](../../COURS/2_sets/#lets-talk-about-sets)

Appartenance - Inclusion - Égalité - Type `Set` en python - Définition par extension/compréhension -
Ensembles de  nombres -  Cardinal d'un  ensemble fini -  Ensemble des  parties -
Opérations  sur  les  ensembles  -  Duale  d'une  équation  sur  $\scr  P(E)$  -
Partition  -  Produit  cartésien  -   Fonction  -  Fonction  totale,  injective,
surjective, bijective.


### [Réels](../../COURS/3_reels/)

Ordre dans  $ℝ$ -  Puissances rationnelles  et réelles -  Second degré  - Valeur
absolue - Partie entière - Majorant, PGE, Sup.


### [Premiers pas en Python](../../INFORMATIQUE/1_Premiers_Pas/#chapitre-1-tout-ce-que-vous-auriez-du-savoir-sur-python)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles - Type `list` - Chaînes de caractères.




## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- $\sqrt{2}$ est-il rationnel ? (exercice 1-14)
- 4 propositions de l'[exercice 1-15](../../EXERCICES/1_Bases_logique/#logique)
- Énoncer et démontrer par récurrence la formule donnant la somme des carrés des
  entiers de $1$ à $n$.
- Caractériser à  l'aide de quantificateurs l'injectivité et  la non injectivité
  d'une fonction. Memes questions pour la surjectivité.
-  Donner et  démontrer l'encadrement  d'amplitude  1 de  $\lfloor x\rfloor$  en
  fonction de $x$. (Recherche 3-3)
- $f$ est une  fonction totale de $E$ dans $F,$ $A$ et  $B$ sont deux parties de
  $E$ et $A^{\prime }$ et $B^{\prime }$ sont deux parties de $F.$ Démontrer : 
        
	 - $A\subseteq B⟹ f(A)\subseteq f(B)$
		
	 - $f(A\cup B)=f(A)\cup f(B)$

## Exercices

On évitera de poser un exercice sur majorants/maximum/borne sup.
