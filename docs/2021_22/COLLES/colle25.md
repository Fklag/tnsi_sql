---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 25

**Semaine du 9 au 13 mai 2022**

## Cours



### [Applications linéaires (milieu)](../../LATEX/Poly_AppLin21.pdf)

DIMENSION QUELCONQUE Définition - Image d'une CL - Conservation du neutre - Image d'une famille liée
/ libre  - Caractérisation d'une  AL - $\mathscr  L(V,W)$ - Compositions  d'AL -
Noyau  -  Noyau  et  injectivité  -  Image  -  Image  et  surjectivité  -  Image
réciproque -  Homomorphismes particuliers - Puissances  d'endomorphismes - Image
d'une   famille  libre/génératrice   -   DIMENSION   FINIE  Détermination   d'un
homomorphisme à l'aide de l'image d'une base - Caractérisation des isomorphismes
(image  d'une  base  est  une  base)  - Deux  espaces  isomorphes  ont  la  même
dimension  -  Rang d'un  homomorphisme  -  Théorème  du  rang -  Injectivité  et
surjectivité en  dimension finie -  Noyau d'une  forme linéaire -  Matrice d'une
famille  dans une  base -  Matrice  d'un homomorhisme  - Image  d'un vecteur  et
produit matriciel.

### Python
 

#### [Tris et dictionnaires](../../INFORMATIQUE/8_tris_1/)

Tri  à bulle  - Tri  par insertion  - Tri  par sélection  - Tri  par comptage  -
Dictionnaires. 


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Tri par sélection. Savoir le commenter.
- Tri par comptage. Savoir le commenter.
- Fonction `compte-repets`.
- Composée de deux AL (énoncé et démonstration)
- Caractérisation des AL (énoncé et démonstration).
- Image d'une famille libre par une AL.
- Caractérisation des isomorphismes en dimension finie.


## Exercices


Vous pouvez  poser en colle  tout exercice  de la liste  du chapitre  23
 du  Poly qui  correspond au  cours vu  (pas d'AL  canoniquement associée  à une
 matrice  en  particulier)  et  du   TP  d'info  (en  particulier  exercices  de
 manipulation de dictionnaires).
