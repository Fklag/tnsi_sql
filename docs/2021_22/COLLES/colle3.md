---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 3

**Semaine du 4 au 8 octobre 2021**

## Cours



### [Réels](../../COURS/3_reels/)

Ordre dans  $ℝ$ -  Puissances rationnelles  et réelles -  Second degré  - Valeur
absolue - Partie entière - Majorant, PGE, Sup.

### [Trigonométrie](../../COURS/4_trigo/)

Cercle trigonométrique -  Radian - Mesure de l'angle orienté  de deux vecteurs -
Mesure principale -  Calculs modulo $2π$ - Approche dans  le triangle rectangles
des lignes trigonométriques (sin, cos, tan,  cotan, sec, cosec) - Définition sur
sinus et du cosinus d'un réel - Angles  associés ($-x$, $π\pm x$, $π/2 \pm x$) -
Formulaire  de trigonométrie  (attention examinateurs  : que  celles directement
déduites de  cos(a - b)  ) - Démonstration  de la formule  donnant cos(a -  b) à
l'aide du  produit scalaire  (défini à l'aide  du cosinus)  Parité, périodicité,
courbe  représentative des  fonctions  sin  et cos  (pas  d'études de  fonctions
trigonométriques quelconques dans ce chapitre) -  Résolution de $cos x = \cos α$
(et leurs  équivalents avec sin et  tan) - Définition de  Arccos, Arcsin, Arctan
(on a évoqué les propriétés algébriques mais n'a pas étudié les fonctions).


### [Python](../../INFORMATIQUE/1_Premiers_Pas/#chapitre-1-tout-ce-que-vous-auriez-du-savoir-sur-python)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles - Type `list` - Chaînes de caractères - Boucles - Définition de fonctions.




## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:


- Tout extrait du [DS1](../../DS/DS1_21.pdf) (sauf l'exercice VI qui n'a pas été demandé).
- Donner et démontrer l'encadrement d'amplitude 1 de $\lfloor x\rfloor$ en
  fonction de $x$. (Recherche 3-3)
- Connaissant  la formule donnant  $\cos(a -b)$, démontrer les  formules donnant
  $\sin(a-b)$, $\tan(a-b)$ et $cos(2x)$.
- Démontrer la formule donnant $\cos(a -b)$ en utilisant le produit scalaire.
- 	On donne $
	   \cos x = {\sqrt {2 + \sqrt 2}\over 2}
		  \qquad {\rm avec} \qquad
	   x \in I = \left [ 0; {\pi \over 4}\right] .
	$.  Calculer $\sin x$ puis $\sin 2x$.
-  Soit $f:x\mapsto |2x+6|-|x-2|$.
	Donner  une expression  de $f$  comme fonction  affine par  morceaux. Tracer
	l'allure de la courbe représentative dans un repère bien choisi. 
- $9^x+3^x-12=0$
- $\sqrt{x+2}=x-4$
- $(m^2-1)x=m+1\quad (m\inℝ)$
- Donnez  un code  en *Python*  construisant une liste  `F` contenant  deux fois
  l'entier `1` suivis de `n - 2` fois 
  l'entier `0`. Transformez cette liste  pour qu'elle contienne les $n$ premiers
  termes de la suite de Fibonacci. 


## Exercices

On  insistera sur  les calculs  et  la présentation  rigoureuse des  résolutions
d'(in)équations.



