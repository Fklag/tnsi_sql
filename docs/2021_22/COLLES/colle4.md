---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 4

**Semaine du 11 au 15 octobre 2021**

## Cours


### [Trigonométrie](../../COURS/4_trigo/)

Cercle trigonométrique -  Radian - Mesure de l'angle orienté  de deux vecteurs -
Mesure principale -  Calculs modulo $2π$ - Approche dans  le triangle rectangles
des lignes trigonométriques (sin, cos, tan,  cotan, sec, cosec) - Définition sur
sinus et du cosinus d'un réel - Angles  associés ($-x$, $π\pm x$, $π/2 \pm x$) -
Formulaire  de trigonométrie  (attention examinateurs  : que  celles directement
déduites de  cos(a - b)  ) - Démonstration  de la formule  donnant cos(a -  b) à
l'aide du  produit scalaire  (défini à l'aide  du cosinus)  Parité, périodicité,
courbe  représentative des  fonctions  sin  et cos  (pas  d'études de  fonctions
trigonométriques quelconques dans ce chapitre) -  Résolution de $cos x = \cos α$
(et leurs  équivalents avec sin et  tan) - Définition de  Arccos, Arcsin, Arctan
(on a évoqué les propriétés algébriques mais n'a pas étudié les fonctions).


### [Complexes](../../LATEX/PolyComplexe21_p.pdf)

Approche historique via  Caspar WESSEL - Définition de $ℂ$  - Forme algébrique -
Conjugué  -  Module  -  Résolution  de  $X^2=α$  dans  le  cas  $α∈ℝ$  et  aussi
$α∈ℂ\setminus ℝ$ à  titre d'exercice - Résolution d'équations du  second degré à
coefficients  réels  et  aussi  à coefficients  complexes  quelconques  à  titre
d'exercice en étant guidé.


### [Python](../../INFORMATIQUE/1_Premiers_Pas/#chapitre-1-tout-ce-que-vous-auriez-du-savoir-sur-python)

Vocabulaire   de  base   -  Types   de  base   -  Instructions   et  expressions
conditionnelles - Type `list` - Chaînes de caractères - Boucles - Définition de fonctions.




## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:


- Connaissant  la formule donnant  $\cos(a -b)$, démontrer les  formules donnant
  $\sin(a-b)$, $\tan(a-b)$ et $cos(2x)$.
- Démontrer la formule donnant $\cos(a -b)$ en utilisant le produit scalaire.
- 	On donne $
	   \cos x = {\sqrt {2 + \sqrt 2}\over 2}
		  \qquad {\rm avec} \qquad
	   x \in I = \left [ 0; {\pi \over 4}\right] .
	$.  Calculer $\sin x$ puis $\sin 2x$.
- Démontrer l'unicité de l'écriture sous forme algébrique d'un élément de $ℂ$.
- Démontrer que  le conjugué d'un produit  est égal au produit  des conjugués et
  que le conjugué de l'inverse est l'inverse du conjugué.
- Tout extrait des exercices 5.9, 5.11, 5.13-1)
- Donnez un code en *Python* construisant une liste `F` contenant deux fois
  l'entier `1` suivis de `n - 2` fois 
  l'entier `0`. Transformez cette liste  pour qu'elle contienne les $n$ premiers
  termes de la suite de Fibonacci. 


## Exercices

On  insistera sur  les calculs  et  la présentation  rigoureuse des  résolutions
d'(in)équations.



