---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 18

**Semaine  du 7 au 11 mars 2022**

## Cours





### [Espaces vectoriels (début)](../../LATEX/PolyEV21.pdf)

Définition d'un $\mathbb K$-espace vectoriel (pour information - pas d'exercices
dessus). Cinq exemples de référence. Sous-espaces vectoriels - Caractérisations. 
SEV engendré  par une  famille - Famille  génératrice - Famille  libre -  Base -
Dimension  - Une  famille libre  (ou génératrice)  de cardinal  la dimension  de
l'espace est une base - Bases  canoniques de $\mathbb K^n$, de $\mathbb K_n[X]$,
de $\mathfrak M_n(\mathbb K)$.




### [Suites (suite)](../../LATEX/PolySuitesConv21.pdf)

Suites bornées  - Sens de  variation - Convergence,  divergence - Unicité  de la
limite  - Toute  suite convergente  est bornée  - Opérations  sur les  limites -
Passage  à la  limite  des inégalités  - Théorème  d'encadrement  des limites  -
Théorème de  comparaison des limites  infinies - Suite croissante  convergente -
Théorème des  suites monotones -  Croissances comparées - Suites  équivalentes -
Définition - Caractérisation par la limite  du rapport - Rapports avec le signe,
les limites  - Opérations sur les  équivalents - Équivalents usuels  à partir de
suites convergeant vers 0 (limites de taux de variations) - Limite de $(1 + 1/n)^n$.



### Python
 
#### [Manipulation d'images](../../INFORMATIQUE/TP_image/TP_image/)

 Manipulation d'images  en tant que  matrice : jusqu'à la  luminance (changement
 d'énergie non compris).



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

-  Soit  $E$  l'ensemble  des  suites  réelles  qui  vérifient  la  relation  de
    récurrence: $\forall n \in \mathbb N$, $ u _{n+2} = u_{n+1} + u_n $.
	1. Montrer que $E$ est un espace vectoriel.
    2.  En déterminer une base.
- Soit  $f$ et $g$ les  fonctions définies sur $\mathbb  R$ par $ \forall  x \in
  \mathbb R $, $
  f(x) =  e^x $ et  $ g(x) =  e^{2x} $. La  famille $(f,g)$ est-elle  libre dans
  l'espace vectoriel des fonctions réelles? 
- Donner une base du SEV de $\mathfrak{M}_4(\mathbb R)$ des matrices qui sont
  antisymétriques.
- $\displaystyle \lim_{n\to+\infty}\left(1+\dfrac{1}{n} \right)^n$.
- $\displaystyle\lim_{n\to+\infty}s_n$ avec $s_n=\displaystyle\sum_{k=1}^n\dfrac{n}{n^2+k}$ et fonction
Python donnant une approximation de $s_n$ pour un entier $n$ donné en paramètre.


## Exercices


Vous pouvez  poser en colle  tout exercice  de la liste  des chapitres 17  et 18
(sauf  suites  adjacentes)  du  Poly.  On évitera  les  exercices  exigeant  des
démonstrations « avec des $ε$ ».

