---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 6

**Semaine du 8 au 12 novembre 2021**

## Cours



### [Complexes](../../LATEX/PolyComplexe21_p.pdf)

Approche historique via  Caspar WESSEL - Définition de $ℂ$  - Forme algébrique -
Conjugué  -  Module  -  Résolution  de  $X^2=α$  dans  le  cas  $α∈ℝ$  et  aussi
$α∈ℂ\setminus ℝ$ à  titre d'exercice - Résolution d'équations du  second degré à
coefficients  réels  et  aussi  à coefficients  complexes  quelconques  à  titre
d'exercice  en étant  guidé  - Formes  trigonométriques  et exponentielles  d'un
nombre complexe.

### [Dénombrements](../../LATEX/PolyDenomb21_p.pdf)

Calculs  de sommes  et  de  produits -  Notations  Σ et  Π  -  Sommes doubles  -
**Cardinaux**  de  ${\cal   P}(E)$,  d'un  produit  cartésien,   d'une  réunion  -
**Dénombrements des  ensembles**: nombre de  $p$-listes (ou $p$-uplets)  avec et
sans  répétitions -  Arrangements -  Permutations -  Combinaisons -  Triangle de
Pascal - Formule du binôme. 


### [Python](../../INFORMATIQUE/2_Premiers_exos/)

Exemples de boucles : calculs de sommes et de produits - compteurs.



## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:


- Tout extrait du [DS3](../../DS/DS3_21.pdf)
- Forme exponentielle de $1-e^{iα}$
- $\displaystyle\sum_{k=0}^n\cos(kx)$
- Calcul de $\displaystyle \sum_{i=1}^p\left(\sum_{k=1}^ik×i\right)$.
- Relation de Pascal: démonstration ensembliste et par le calcul.
- Calcul de $ \displaystyle\sum _{k=0} ^n k \binom{n}{k} $.

## Exercices

On  insistera sur  les calculs  et  la présentation  rigoureuse des  résolutions
d'(in)équations.



