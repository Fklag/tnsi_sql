---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 24

**Semaine du 2 au 6 mai 2022**

## Cours


### [Intégration 2](../../LATEX/PolyInt2_21.pdf)

Rappels sur  le chapitre 10 (définition  en termes d'aire, relation  de Chasles,
linéarité,  IPP, changement  de variable)  -  Intégrales et  relation d'ordre  -
Valeur moyenne  - Théorème fondamental de  l'analyse - Fonction définie  par une
intégrale -  Somme de Riemann :  théorème admis pour les  fonctions continues et
démonstration  pour des  fonctions  de classe  $\mathscr  C^1$ -  Interprétation
géométrique.

### [Applications linéaires (début)](../../LATEX/Poly_AppLin21.pdf)

Définition - Image d'une CL - Conservation  du neutre - Image d'une famille liée
/ libre  - Caractérisation d'une  AL - $\mathscr  L(V,W)$ - Compositions  d'AL -
Noyau - Noyau et injectivité.

### Python
 

#### [Tris](../../INFORMATIQUE/8_tris_1/)

Tri à bulle - Tri par insertion - Tri par sélection - Tri par comptage.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Tri à bulle. Savoir le commenter.
- Tri par sélection. Savoir le commenter.
- Tri par comptage. Savoir le commenter.
- Convergence  des sommes  de Riemann  : démonstration de  la convergence  de la
  suite $(S_n)$ dans le cas d'une fonction $\mathscr C^1$ sur un intervalle $[a,b]$.
-     Sens     de     variation      de     la     fonction     $G:     x\mapsto
  \displaystyle\int_x^{2x}\dfrac{dt}{t^2+t+1}$.
- Composée de deux AL (énoncé et démonstration)
- Caractérisation des AL (énoncé et démonstration).
- Image d'une famille libre par une AL.
- Lien entre noyau et injectivité (énoncé et preuve)

## Exercices


Vous pouvez  poser en colle  tout exercice  de la liste  des chapitres 22  et 23
 du Poly et du TP d'info jusqu'au tri par comptage.
