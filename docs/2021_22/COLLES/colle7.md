---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 7

**Semaine du 15 au 19 novembre 2021**

## Cours



### [Dénombrements](../../LATEX/PolyDenomb21.pdf)

Calculs  de sommes  et  de  produits -  Notations  Σ et  Π  -  Sommes doubles  -
**Cardinaux**  de  ${\cal   P}(E)$,  d'un  produit  cartésien,   d'une  réunion  -
**Dénombrements des  ensembles**: nombre de  $p$-listes (ou $p$-uplets)  avec et
sans  répétitions -  Arrangements -  Permutations -  Combinaisons -  Triangle de
Pascal - Formule du binôme. 


### [Généralités sur les suites réelles](../../LATEX/PolySuites_1_21.pdf)

Définitions    -    Opérations    -    Suites    arithmétiques,    géométriques,
arithmético-géométriques,   récurrentes  linéaires   d'ordre  2   (formule  sans
démonstration).


### [Python](../../INFORMATIQUE/2_Premiers_exos/)

Exemples de  boucles : calculs de  sommes et de  produits - compteurs -  test de
présence de caractères dans une chaîne - Recherches de maximum.  (ex 2-7 2-8 2-9)
On pourra demander en exercice des calculs de dénombrement :  nombre
d'arrangements, de permutations, de combinaisons.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous:

- Déterminez une fonction qui prend une  liste de nombres en argument et renvoie
  un couple contenant l'indice de la première occurence du maximum et sa valeur. 
- Tout extrait du [DS3](../../DS/DS3_21.pdf)
- Relation de Pascal: démonstration ensembliste et par le calcul.
- Calcul de $ \displaystyle\sum _{k=0} ^n k \binom{n}{k} $.
-  $ u_0  = 2  $ et  $ \forall  n  \in ℕ$,  $ u_{n+1}  = -2  u_n +  3 $.  Donner
  l'expression de $u_n$ en fonction de $n$. 
-    On    considère    la    suite   $(u_n)$    définie    par    :    $u_0=1$,
  $u_1=\sqrt{3}-\dfrac{1}{2}$     et      $\forall     n     \in      ℕ,     \:
  u_{n+2}=-u_{n+1}-u_{n}$. Exprimer son terme général. 
- Formule et  démonstration du calcul  de la somme des termes successifs d'une
  suite arithmétique.

## Exercices

On posera au moins un exercice impliquant l'écriture d'une fonction en Python.

Vous pouvez poser en colle tout exercice de la liste du chapitre 7 du Poly.

