---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 21

**Semaine du 28 mars au 1er avril 2022**

## Cours

### [Limites et continuité](../../LATEX/PolyLimCont21.pdf)

Notion de  voisinage - Définitions  des différentes  limites - Continuité  en un
réel - Opérations sur les limites -
Composition -  Relation d'ordre  - Encadrement -  Prolongement par  continuité -
Limites et  suites - Théorème de  la limite monotone -  Fonctions équivalentes -
Opérations sur les  équivalents - Équivalents usuels - Fonction  continue sur un
intervalle - Fonctions continues par morceaux  - TVI - Image d'un intervalle par
une  fonction continue  -  Théoreme des  bornes  - Théorème  de  la bijection  -
Arctan - Résolution d'équations numériques par dichotomie. 


### [Dérivation : approfondissement](../../LATEX/PolyDerApp21.pdf)

Révisions  sur  les  chapitres  8  et 9  (nombre  dérivé,  calcul  de  dérivées,
dérivation et  variations) - Dérivées  successives :  la formule de  Leibniz est
hors  programme  mais a  été  vue  (on peut  la  rappeler  pour un  exercice)  -
Dérivation et extrema  - Théorème de Rolle - Égalité  des accroissements finis -
L'inégalité ne doit pas être sue mais  doit pouvoir être redémontrée à partir de
l'EAF - Application aux suites définies par une relation de récurrence.



### Python
 
#### [Dichotomie](../../INFORMATIQUE/7_Dichotomie)

 Recherche  séquentielle  - Recherche  dichotomique  -  Résolution d'équation  -
 Exemple de recherche d'une valeur approchée de $\sqrt{2}$.

#### Tris

Tri à bulle.


## Questions de cours

On commencera la colle par une des questions proposées ci-dessous :

- Fonction Python `dicho(f, a, b,  eps)` calculant une approximation de $f(x)=0$
  sur un intervalle 
  $[a,b]$ où $f$ est continue et strictement croissante avec une précision
  arbitraire   `eps`   (supérieure   à   $2^{52}$)  à   l'aide   d'une   méthode
  dichotomique. En donner une version récursive et une autre impérative.
- Équivalent de $\cos(x) - 1$ au voisinage de 0 : énoncé et preuve.
- $\displaystyle\lim_{x\to2}\dfrac{\sqrt{x^2+1}-\sqrt{x+3}}{x^2-2x}$.
-  Soit  $f$  une fonction  continue  de  $[0,1]$  et  à valeurs  dans  ce  même
  intervalle. Démontrer qu'il existe au moins un réel $x_0∈[0,1]$ vérifiant $f(x_0)=x_0$.
- Tri à bulle.
-  Égalité des  accroissements finis  : énoncé  et démonstration.  Application à
  l'inégalité des accroissements finis.
- Théorème de Rolle : énoncé et démonstration.
- Dérivée de $\ln\circ\ln\circ\cdots\circ\ln$ ($n$ compositions avec $n∈ℕ$).

## Exercices


Vous pouvez  poser en colle  tout exercice  de la liste  des chapitres 19  et 20
 du Poly.
